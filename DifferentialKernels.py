"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

from typing import Tuple

import numpy as np

def getKroonParameters(version: int = 0) -> np.ndarray:

    # Values from paper
    p = np.array([0.008,
                  0.049,
                  0.032,
                  0.038,
                  0.111,
                  0.448,
                  0.081,
                  0.334,
                  0.937,
                  0.001,
                  0.028,
                  0.194,
                  0.006,
                  0.948], dtype = np.double)

    if version == 1:
        # Values from code, unrounded values from paper
        p = np.array([0.007520981141059,
                      0.049564810649554,
                      0.031509665995882,
                      0.037869547950557,
                      0.111394943940548,
                      0.448053798986724,
                      0.081135611356868,
                      0.333881751894069,
                      0.936734100573009,
                      0.000936487500442,
                      0.027595332069424,
                      0.194217089668822,
                      0.006184018622016,
                      0.948352724021832], dtype = np.double)
    elif version == 2:
        # Used values from code, updated
        p = np.array([0.00549691757211334,
                      4.75686155670860e-10,
                      3.15405721902292e-11,
                      0.00731109320628158,
                      1.40937549145842e-10,
                      0.0876322157772825,
                      0.0256808495553998,
                      5.87110587298283e-11,
                      0.171008417902939,
                      3.80805359553021e-12,
                      9.86953381462523e-12,
                      0.0231020787600445,
                      0.00638922328831119,
                      0.0350184289706385], dtype=np.double)

    return p.flatten()

def getFiniteDifferenceKernels() -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    kx = (1.0/2.0) * np.array([[ 0.0, 0.0, 0.0],
                               [-1.0, 0.0, 1.0],
                               [ 0.0, 0.0, 0.0]])

    kxx = (1.0/4.0) * np.array([[ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0, 1.0,-2.0, 1.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0]])

    kxy = (1.0/4.0) * np.array([[ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0,-1.0, 0.0, 1.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0, 1.0, 0.0,-1.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0]])
    kx  = -kx
    kxy = -kxy
    ky  = kx.transpose()
    kyy = kxx.transpose()
    return kx, ky, kxx, kyy, kxy

def getSobelKernels() -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    kx = (1.0/8.0) * np.array([[-1.0, 0.0, 1.0],
                               [-2.0, 0.0, 2.0],
                               [-1.0, 0.0, 1.0]])

    kxx = (1.0/64.0) * np.array([[ 1.0, 0.0, -2.0, 0.0, 1.0],
                                 [ 4.0, 0.0, -8.0, 0.0, 4.0],
                                 [ 6.0, 0.0,-12.0, 0.0, 6.0],
                                 [ 4.0, 0.0, -8.0, 0.0, 4.0],
                                 [ 1.0, 0.0, -2.0, 0.0, 1.0]])

    kxy = (1.0/64.0) * np.array([[-1.0,-2.0, 0.0, 2.0, 1.0],
                                 [-2.0,-4.0, 0.0, 4.0, 2.0],
                                 [ 0.0, 0.0, 0.0, 0.0, 0.0],
                                 [ 2.0, 4.0, 0.0,-4.0,-2.0],
                                 [ 1.0, 2.0, 0.0,-2.0,-1.0]])
    kx  = -kx
    kxy = -kxy
    ky  = kx.transpose()
    kyy = kxx.transpose()
    return kx, ky, kxx, kyy, kxy

def getScharrKernels() -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    kx = (1.0/32.0) * np.array([[ -3.0, 0.0,  3.0],
                                [-10.0, 0.0, 10.0],
                                [ -3.0, 0.0,  3.0]])

    kxx = (1.0/1024.0) * np.array([[   9.0, 0.0,  -18.0, 0.0,   9.0],
                                   [  60.0, 0.0, -120.0, 0.0,  60.0],
                                   [ 118.0, 0.0, -236.0, 0.0, 118.0],
                                   [  60.0, 0.0, -120.0, 0.0,  60.0],
                                   [   9.0, 0.0, - 18.0, 0.0,   9.0]])

    kxy = (1.0/1024.0) * np.array([[ -9.0, -30.0, 0.0,  30.0,  9.0],
                                   [-30.0,-100.0, 0.0, 100.0, 30.0],
                                   [  0.0,   0.0, 0.0,   0.0,  0.0],
                                   [ 30.0, 100.0, 0.0,-100.0,-30.0],
                                   [  9.0,  30.0, 0.0, -30.0,-9.0]])
    kx  = -kx
    kxy = -kxy
    ky  = kx.transpose()
    kyy = kxx.transpose()
    return kx, ky, kxx, kyy, kxy

def getKroonKernels(version: int=0) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:

    p = getKroonParameters(version)

    # The papers by Kroon as well as the matlab code starts indexing from 1 so
    # do expand array into variables for easier readability
    p01 = p[0]
    p02 = p[1]
    p03 = p[2]
    p04 = p[3]
    p05 = p[4]
    p06 = p[5]
    p07 = p[6]
    p08 = p[7]
    p09 = p[8]
    p10 = p[9]
    p11 = p[10]
    p12 = p[11]
    p13 = p[12]
    p14 = p[13]

    kx = np.array([[-p13, 0, p13],
                   [-p14, 0, p14],
                   [-p13, 0, p13]], dtype=float)

    kxx = np.array([[p01, p04, -p07, p04, p01],
                    [p02, p05, -p08, p05, p02],
                    [p03, p06, -p09, p06, p03],
                    [p02, p05, -p08, p05, p02],
                    [p01, p04, -p07, p04, p01]], dtype=float)

    kxy = np.array([[ p10,  p11, 0, -p11, -p10],
                    [ p11,  p12, 0, -p12, -p11],
                    [   0,    0, 0,    0,    0],
                    [-p11, -p12, 0,  p12,  p11],
                    [-p10, -p11, 0,  p11,  p10]], dtype=float)

    kx  = -kx
    ky  = kx.transpose()
    kyy = kxx.transpose()
    return kx, ky, kxx, kyy, kxy
