
import numpy as np

def noisy(noise_typ: str, image: np.ndarray):
    """
        Parameters
        ----------
        image : ndarray
            Input image data. Will be converted to float.
        mode : str
            One of the following strings, selecting the type of noise to add:

            'gauss'     Gaussian-distributed additive noise.
            'poisson'   Poisson-distributed noise generated from the data.
            's&p'       Replaces random pixels with 0 or 1.
            'speckle'   Multiplicative noise using out = image + n*image,where
                        n is uniform noise with specified mean & variance.
    """
    if noise_typ == "gauss":
        #row, col, ch= image.shape
        shape = image.shape
        row = shape[0]
        col = shape[1]
        ch = 1
        if len(shape)>2:
            ch = shape[2]
        else:
            image = np.reshape(image, (row,col,ch))
        #print(row)
        #print(col)
        #print(ch)
        mean = 0
        var = 0.01
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        #print(gauss.shape)
        #print(image.shape)
        #gauss = gauss.reshape(row,col,ch)
        #print(gauss.shape)
        noisy = image + gauss
        print(noisy.shape)
        return noisy
    elif noise_typ == "s&p":
        row,col,ch = image.shape
        s_vs_p = 0.5
        amount = 0.004
        out = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt)) for i in image.shape]
        out[coords] = 1

        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper)) for i in image.shape]
        out[coords] = 0
        return out
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        return noisy
    elif noise_typ =="speckle":
        shape = image.shape
        row = shape[0]
        col = shape[1]
        ch = 1
        if len(shape)>2:
            ch = shape[2]
        else:
            image = np.reshape(image, (row,col,ch))
        gauss = np.random.randn(row,col,ch)
        gauss = gauss.reshape(row,col,ch)
        noisy = image + image * gauss
        return noisy

