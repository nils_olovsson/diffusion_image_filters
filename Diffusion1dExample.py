#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

import os
import shutil

from typing import Tuple, List

import math

import numpy as np
import scipy.signal

import skimage.io

import matplotlib.pyplot as plt
import matplotlib.lines as mlines

import FileSystem
import LatexPreamble

def linearDiffision1D(
    image: np.ndarray,
    iterations: int = 100,
    g: float = 1,
    tau0: float = 0.250*0.25,
    tau1: float = 0.250,
    image_seq = None,
):
    """
        Execute nonlinear, isotropic, smoothing filter on 1D image.
    """

    def computeUpdate(u: np.ndarray, g: float):
        """
            Compute the update for the next iteration using the spatial
            derivatives.
        """
        update = np.zeros(u.shape, dtype=float)
        u = np.pad(u, pad_width=1, mode='constant')

        for i in range(1, u.shape[0]-1):
                g_p = g
                g_n = g
                if i==u.shape[0]-2:
                    g_p = 0
                if i==1:
                    g_n = 0
                ux0 =   g_p * (u[i+1] - u[i])
                ux1 = - g_n * (u[i]   - u[i-1])
                update[i-1] = ux0 + ux1
        return update

    u = np.copy(image)
    if image_seq != None:
        image_seq.append(np.copy(u))

    for i in range(iterations):
        print(f"Iteration: {i+1}/{iterations}")
        a = i / (iterations-1)
        tau = (1.0 - a) * tau0 + a * tau1
        update = computeUpdate(u, g)
        u = u + tau * update
        if image_seq != None:
            image_seq.append(np.copy(u))
    return u

def inhomogeneousDiffision1D(
        image: np.ndarray,
        iterations: int,
        g: np.ndarray,
        tau0: float = 0.250*0.25,
        tau1: float = 0.250,
        image_seq = None
    ):
    """
        Execute nonlinear, isotropic, smoothing filter on 1D image.
    """

    def computeUpdate(u: np.ndarray, g: np.ndarray):
        """
            Compute the update for the next iteration using the spatial
            derivatives.
        """
        update = np.zeros(u.shape, dtype=float)
        u = np.pad(u, pad_width=1, mode='constant')
        g = np.pad(g, pad_width=1, mode='constant')

        for i in range(1, u.shape[0]-1):
            g_p = math.sqrt(g[i+1] * g[i])
            g_n = math.sqrt(g[i-1] * g[i])
            if i==u.shape[0]-2:
                g_p = 0
            if i==1:
                g_n = 0
            ux0 =   g_p * (u[i+1] - u[i])
            ux1 = - g_n * (u[i]   - u[i-1])

            update[i-1] = ux0 + ux1
        return update

    u = np.copy(image)
    if image_seq != None:
        image_seq.append(np.copy(u))

    for i in range(iterations):
        print(f"Iteration: {i+1}/{iterations}")
        a = i / (iterations-1)
        tau = (1.0 - a) * tau0 + a * tau1
        update = computeUpdate(u, g)
        u = u + tau * update
        if image_seq != None:
            image_seq.append(np.copy(u))
    return u

def nonlinearDiffision1D(
        image: np.ndarray,
        iterations: int,
        lamb: float,
        tau0: float = 0.250*0.25,
        tau1: float = 0.250,
        image_seq = None
    ):
    """
        Execute nonlinear, isotropic, smoothing filter on 1D image.
    """

    def computeDiffusivity(u: np.ndarray, lamb: float):
        """
        """
        gradkernelx = 0.5 * np.array([-1.0, 0.0, 1.0])
        gradx  = scipy.signal.convolve(u, gradkernelx)
        gradm2 = np.power(gradx, 2)
        g = 1.0 / np.sqrt(1.0 + gradm2 / (lamb*lamb))
        return g

    def computeUpdate(u: np.ndarray, lamb: float):
        """
            Compute the update for the next iteration using the spatial
            derivatives.
        """
        update = np.zeros(u.shape, dtype=float)
        u = np.pad(u, pad_width=1, mode='constant')
        g = computeDiffusivity(u, lamb)

        for i in range(1, u.shape[0]-1):
            g_p = math.sqrt(g[i+1] * g[i])
            g_n = math.sqrt(g[i-1] * g[i])
            if i==u.shape[0]-2:
                g_p = 0
            if i==1:
                g_n = 0
            ux0 =   g_p * (u[i+1] - u[i])
            ux1 = - g_n * (u[i]   - u[i-1])

            update[i-1] = ux0 + ux1
        return update

    u = np.copy(image)
    if image_seq != None:
        image_seq.append(np.copy(u))

    for i in range(iterations):
        print(f"Iteration: {i+1}/{iterations}")
        a = i / (iterations-1)
        tau = (1.0 - a) * tau0 + a * tau1
        update = computeUpdate(u, lamb)
        u = u + tau * update
        if image_seq != None:
            image_seq.append(np.copy(u))
    return u

def drawBoundingBox(ax, l, r, d, u, width):

    lines = 4*[None]
    lines[0] = mlines.Line2D([l,r], [d,d])
    lines[1] = mlines.Line2D([r,r], [d,u])
    lines[2] = mlines.Line2D([r,l], [u,u])
    lines[3] = mlines.Line2D([l,l], [u,d])

    for l in lines:
        l.set_color('black')
        l.set_linewidth(width)
        l.set_zorder(5)
        l.set_solid_capstyle('round')
        ax.add_line(l)

def drawHorizontal(ax, l, r, hv, width):
    l = mlines.Line2D([l,r], [hv,hv])
    l.set_color('tab:grey')
    l.set_linewidth(width)
    l.set_linestyle('-')
    l.set_zorder(0)
    l.set_solid_capstyle('butt')
    ax.add_line(l)
    return l

def drawVertical(ax, d, u, vv, width, color='k'):
    l = mlines.Line2D([vv,vv], [d,u])
    l.set_color(color)
    l.set_linewidth(width)
    l.set_linestyle('--')
    l.set_zorder(0)
    l.set_solid_capstyle('butt')
    ax.add_line(l)
    return l

def getSignalAndNoise():
    N = 100
    shape = (N,1)
    seed = 0
    np.random.seed(seed)
    noise = np.random.normal(0.0, 0.2, size=100).flatten()

    x = np.arange(0,100)
    u = 0.2 + 0.2 * 0.01 * x
    u[:30] -= 0.3
    u[30:59] += 0.7
    u[60:] -= 0.4

    u0 = u + noise
    return x, u, u0

def makeFigures0Diffusion1D():

    x, u, u0 = getSignalAndNoise()

    diffusivity = 2.0
    g = diffusivity * np.ones(u.shape).flatten()
    g[30] = 0.0
    g[59] = 0.0

    u_lin_arr = []
    u_inh_arr = []

    iterations = 60 * 60 - 1
    u_lin = linearDiffision1D(u0, iterations, g = diffusivity, image_seq = u_lin_arr)
    u_inh = inhomogeneousDiffision1D(u0, iterations, g = g, image_seq = u_inh_arr)

    s0 = np.sum(u0)
    s1 = np.sum(u_lin)
    s2 = np.sum(u_inh)

    maxv = np.amax(u0)
    minv = np.amin(u0)
    avgv = np.mean(u0)

    print('sum: {}'.format(s0))
    print('sum: {} [{}]'.format(s1, s1/s0))
    print('sum: {} [{}]'.format(s2, s2/s0))

    print('min: {}'.format(minv))
    print('max: {}'.format(maxv))

    directory_img   = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'diffusion1D', 'img'])
    directory_video = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'diffusion1D', 'video'])

    full_sampling = True

    LatexPreamble.latexMatplotlibPreample()

    linewidth = 2.5
    for i in range(0, len(u_lin_arr)):
        if full_sampling or i % 10 == 0:
            fig = plt.figure(figsize=(8,3.2), dpi=175)
            ax: plt.Axes = plt.gca()

            label0 = 'Original'
            label1 = 'Noisy'
            label2 = 'Linear and\nhomogeneous (1)'
            label3 = 'Inhomogeneous (2)\n(Insulated)'

            h0 = ax.plot(x, u,            color = 'tab:blue',   label = label0, lw = linewidth, zorder = 1, linestyle = '--')
            h1 = ax.plot(x, u0,           color = 'k',          label = label1, lw = linewidth, zorder = 4, linestyle = ':')
            h2 = ax.plot(x, u_lin_arr[i], color = 'tab:green',  label = label2, lw = linewidth, zorder = 2)
            h3 = ax.plot(x, u_inh_arr[i], color = 'tab:red', label = label3, lw = linewidth, zorder = 3)

            ax.set_axis_off()

            drawBoundingBox(ax, -0.5, 99, -0.60, 1.5, width=linewidth)

            hv0 = drawVertical(ax, -0.55, 1.5, 30, width=linewidth, color=(0.7, 0.7, 0.7))
            hv1 = drawVertical(ax, -0.55, 1.5, 59, width=linewidth, color=(0.7, 0.7, 0.7))
            hh  = drawHorizontal(ax, 0, 99, avgv, width=linewidth)

            ax.text(61, 1.0, 'Insulation $(g=0)$', va='bottom', ha='left', fontsize=16, color='k')

            handles = []
            labels  = []
            handles, labels = ax.get_legend_handles_labels()
            labels  = list(labels)
            handles = list(handles)
            handles.append(hh)
            labels.append('Signal mean')
            legend = ax.legend(handles=handles,
                               labels=labels,
                               framealpha = 0.0,
                               #bbox_to_anchor=(0.95, 0.05),
                               #loc='lower right',
                               bbox_to_anchor=(0.05, 0.99),
                               loc='upper left',
                               ncol = 1,
                               )
            ax.add_artist(legend)

            filepath = os.path.join(directory_img, f'img-{i:05d}.png')
            plt.savefig(filepath, bbox_inches='tight', pad_inches=0.01)
            #plt.show()
            plt.close()

            loaded_img = skimage.io.imread(filepath)
            shape = loaded_img.shape
            if shape[0]%2 != 0 or shape[1]%2 != 0:
                resx = shape[0]%2
                resy = shape[1]%2

                loaded_img = loaded_img[0:shape[0]-resx, 0:shape[1]-resy,:]
                skimage.io.imsave(filepath, loaded_img)
        if i % 100 == 0:
            print(f"Saving images {100*((i+1)/len(u_lin_arr)):.1f}%")

    sourcedir = os.path.join(directory_img, '*.png')
    targetdir = directory_video
    filename = 'diffusion-1d.mp4'
    filepath = os.path.join(targetdir, filename)
    if full_sampling:
        command = f'ffmpeg -framerate 60 -pattern_type glob -i "{sourcedir}" -pix_fmt yuv420p -r 60 -y {filepath}'
    else:
        command = f'ffmpeg -framerate 6 -pattern_type glob -i "{sourcedir}" -pix_fmt yuv420p -r 60 -y {filepath}'
    print(command)
    os.system(command)

    shutil.rmtree(directory_img)

def makeFigures1Diffusion1D():

    diffusivity = 2.0
    x, u, u0 = getSignalAndNoise()
    u_lin_arr = []
    u_non_arr = []

    iterations = 60 * 60 - 1
    u_lin = linearDiffision1D   (u0, iterations, g = diffusivity, image_seq = u_lin_arr)
    u_non = nonlinearDiffision1D(u0, iterations, lamb=0.001, image_seq = u_non_arr)

    s0 = np.sum(u0)
    s1 = np.sum(u_lin)
    s2 = np.sum(u_non)

    maxv = np.amax(u0)
    minv = np.amin(u0)
    avgv = np.mean(u0)

    print('sum: {}'.format(s0))
    print('sum: {} [{}]'.format(s1, s1/s0))
    print('sum: {} [{}]'.format(s2, s2/s0))

    print('min: {}'.format(minv))
    print('max: {}'.format(maxv))

    directory_img   = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'diffusion1D', 'img-nonlinear'])
    directory_video = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'diffusion1D', 'video'])

    full_sampling = True

    LatexPreamble.latexMatplotlibPreample()

    linewidth = 2.5
    for i in range(0, len(u_lin_arr)):
        if full_sampling or i % 10 == 0:
            fig = plt.figure(figsize=(8,3.2), dpi=175)
            ax: plt.Axes = plt.gca()

            label0 = 'Original'
            label1 = 'Noisy'
            label2 = 'Linear and\nhomogeneous (1)'
            label3 = 'Nonlinear (3)\nPerona and Malik'

            h0 = ax.plot(x, u,            color = 'tab:blue',    label = label0, lw = linewidth, zorder = 1, linestyle = '--')
            h1 = ax.plot(x, u0,           color = 'k', label = label1, lw = linewidth, zorder = 4, linestyle = ':')
            h2 = ax.plot(x, u_lin_arr[i], color = 'tab:green',  label = label2, lw = linewidth, zorder = 2)
            h3 = ax.plot(x, u_non_arr[i], color = 'tab:orange', label = label3, lw = linewidth, zorder = 3)

            ax.set_axis_off()

            drawBoundingBox(ax, -0.5, 99, -0.60, 1.5, width=linewidth)
            hh = drawHorizontal(ax, 0, 99, avgv, width=linewidth)

            handles = []
            labels  = []
            handles, labels = ax.get_legend_handles_labels()
            labels  = list(labels)
            handles = list(handles)
            handles.append(hh)
            labels.append('Signal mean')
            legend = ax.legend(handles=handles,
                               labels=labels,
                               framealpha = 0.0,
                               #bbox_to_anchor=(0.95, 0.05),
                               #loc='lower right',
                               bbox_to_anchor=(0.05, 0.99),
                               loc='upper left',
                               ncol = 1,
                               )
            ax.add_artist(legend)

            filepath = os.path.join(directory_img, f'img-{i:05d}.png')
            plt.savefig(filepath, bbox_inches='tight', pad_inches=0.01)
            #plt.show()
            plt.close()

            loaded_img = skimage.io.imread(filepath)
            shape = loaded_img.shape
            if shape[0]%2 != 0 or shape[1]%2 != 0:
                resx = shape[0]%2
                resy = shape[1]%2

                loaded_img = loaded_img[0:shape[0]-resx, 0:shape[1]-resy,:]
                skimage.io.imsave(filepath, loaded_img)
        if i % 100 == 0:
            print(f"Saving images {100*((i+1)/len(u_lin_arr)):.1f}%")

    sourcedir = os.path.join(directory_img, '*.png')
    targetdir = directory_video
    filename = 'nonlin-diffusion-1d.mp4'
    filepath = os.path.join(targetdir, filename)
    if full_sampling:
        command = f'ffmpeg -framerate 60 -pattern_type glob -i "{sourcedir}" -pix_fmt yuv420p -r 60 -y {filepath}'
    else:
        command = f'ffmpeg -framerate 6 -pattern_type glob -i "{sourcedir}" -pix_fmt yuv420p -r 60 -y {filepath}'
    print(command)
    os.system(command)

    shutil.rmtree(directory_img)

if __name__=='__main__':
    makeFigures0Diffusion1D()
    #makeFigures1Diffusion1D()
