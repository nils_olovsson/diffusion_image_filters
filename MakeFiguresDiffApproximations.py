"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

from abc import ABC, abstractmethod

from typing import Dict, List, Tuple

import numpy as np
import scipy.signal

import sympy

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches

import LatexPreamble

from DiffusionImageFilter import DiffusionImageFilterUpdate

# ==============================================================================
# Implemented differentiations
# ==============================================================================

def getKroonParameters():
    p = np.array([0.008,
                0.049,
                0.032,
                0.038,
                0.111,
                0.448,
                0.081,
                0.334,
                0.937,
                0.001,
                0.028,
                0.194,
                0.006,
                0.948], dtype = np.double)
    return p.flatten()

def computeSobel(Z: np.ndarray):
    Zx = np.zeros(Z.shape, dtype=np.float64)
    Zy = np.zeros(Z.shape, dtype=np.float64)
    Zxx = np.zeros(Z.shape, dtype=np.float64)
    Zxy = np.zeros(Z.shape, dtype=np.float64)
    for r in range(0, Z.shape[0]):
        for c in range(0, Z.shape[1]):
            Zx[r,c]  = DiffusionImageFilterUpdate.sobelSingleChannelX(Z,r,c)
            Zy[r,c]  = DiffusionImageFilterUpdate.sobelSingleChannelY(Z,r,c)
            Zxx[r,c] = DiffusionImageFilterUpdate.sobelSingleChannelXX(Z,r,c)
            Zxy[r,c] = DiffusionImageFilterUpdate.sobelSingleChannelXY(Z,r,c)
    return Zx, Zy, Zxx, Zxy

def computeScharr(Z: np.ndarray):
    Zx = np.zeros(Z.shape, dtype=np.float64)
    Zy = np.zeros(Z.shape, dtype=np.float64)
    Zxx = np.zeros(Z.shape, dtype=np.float64)
    Zxy = np.zeros(Z.shape, dtype=np.float64)
    for r in range(0, Z.shape[0]):
        for c in range(0, Z.shape[1]):
            Zx[r,c]  = DiffusionImageFilterUpdate.scharrSingleChannelX(Z,r,c)
            Zy[r,c]  = DiffusionImageFilterUpdate.scharrSingleChannelY(Z,r,c)
            Zxx[r,c] = DiffusionImageFilterUpdate.scharrSingleChannelXX(Z,r,c)
            Zxy[r,c] = DiffusionImageFilterUpdate.scharrSingleChannelXY(Z,r,c)
    return Zx, Zy, Zxx, Zxy

def computeKroon(Z: np.ndarray):
    p = getKroonParameters()
    Zx = np.zeros(Z.shape, dtype=np.float64)
    Zy = np.zeros(Z.shape, dtype=np.float64)
    Zxx = np.zeros(Z.shape, dtype=np.float64)
    Zxy = np.zeros(Z.shape, dtype=np.float64)
    for r in range(0, Z.shape[0]):
        for c in range(0, Z.shape[1]):
            Zx[r,c]  = DiffusionImageFilterUpdate.kroonSingleChannelX(Z,p,r,c)
            Zy[r,c]  = DiffusionImageFilterUpdate.kroonSingleChannelY(Z,p,r,c)
            Zxx[r,c] = DiffusionImageFilterUpdate.kroonSingleChannelXX(Z,p,r,c)
            Zxy[r,c] = DiffusionImageFilterUpdate.kroonSingleChannelXY(Z,p,r,c)
    return Zx, Zy, Zxx, Zxy

# ==============================================================================
# Kernels
# ==============================================================================

def getFiniteDifferenceKernels():
    kx = (1.0/2.0) * np.array([[ 0.0, 0.0, 0.0],
                               [-1.0, 0.0, 1.0],
                               [ 0.0, 0.0, 0.0]])

    kxx = (1.0/4.0) * np.array([[ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0, 1.0,-2.0, 1.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0]])

    kxy = (1.0/4.0) * np.array([[ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0,-1.0, 0.0, 1.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0],
                                [ 0.0, 1.0, 0.0,-1.0, 0.0],
                                [ 0.0, 0.0, 0.0, 0.0, 0.0]])
    kx = -kx
    kxy = -kxy
    ky = kx.transpose()
    return kx, ky, kxx, kxy

def getSobelKernels():
    kx = (1.0/8.0) * np.array([[-1.0, 0.0, 1.0],
                               [-2.0, 0.0, 2.0],
                               [-1.0, 0.0, 1.0]])

    kxx = (1.0/64.0) * np.array([[ 1.0, 0.0, -2.0, 0.0, 1.0],
                                 [ 4.0, 0.0, -8.0, 0.0, 4.0],
                                 [ 6.0, 0.0,-12.0, 0.0, 6.0],
                                 [ 4.0, 0.0, -8.0, 0.0, 4.0],
                                 [ 1.0, 0.0, -2.0, 0.0, 1.0]])

    kxy = (1.0/64.0) * np.array([[-1.0,-2.0, 0.0, 2.0, 1.0],
                                 [-2.0,-4.0, 0.0, 4.0, 2.0],
                                 [ 0.0, 0.0, 0.0, 0.0, 0.0],
                                 [ 2.0, 4.0, 0.0,-4.0,-2.0],
                                 [ 1.0, 2.0, 0.0,-2.0,-1.0]])
    kx = -kx
    kxy = -kxy
    ky = kx.transpose()
    return kx, ky, kxx, kxy

def getScharrKernels():
    kx = (1.0/32.0) * np.array([[ -3.0, 0.0,  3.0],
                                [-10.0, 0.0, 10.0],
                                [ -3.0, 0.0,  3.0]])

    kxx = (1.0/1024.0) * np.array([[   9.0, 0.0,  -18.0, 0.0,   9.0],
                                   [  60.0, 0.0, -120.0, 0.0,  60.0],
                                   [ 118.0, 0.0, -236.0, 0.0, 118.0],
                                   [  60.0, 0.0, -120.0, 0.0,  60.0],
                                   [   9.0, 0.0, - 18.0, 0.0,   9.0]])

    kxy = (1.0/1024.0) * np.array([[ -9.0, -30.0, 0.0,  30.0,  9.0],
                                   [-30.0,-100.0, 0.0, 100.0, 30.0],
                                   [  0.0,   0.0, 0.0,   0.0,  0.0],
                                   [ 30.0, 100.0, 0.0,-100.0,-30.0],
                                   [  9.0,  30.0, 0.0, -30.0,-9.0]])
    kx = -kx
    kxy = -kxy
    ky = kx.transpose()
    return kx, ky, kxx, kxy

def getKroonKernels():

    # Values from paper
    p = np.array([0.008,
                  0.049,
                  0.032,
                  0.038,
                  0.111,
                  0.448,
                  0.081,
                  0.334,
                  0.937,
                  0.001,
                  0.028,
                  0.194,
                  0.006,
                  0.948], dtype = np.double)

    
    # Values from code, unrounded values from paper
    p = np.array([0.007520981141059,
                  0.049564810649554,
                  0.031509665995882,
                  0.037869547950557,
                  0.111394943940548,
                  0.448053798986724,
                  0.081135611356868,
                  0.333881751894069,
                  0.936734100573009,
                  0.000936487500442,
                  0.027595332069424,
                  0.194217089668822,
                  0.006184018622016,
                  0.948352724021832], dtype = np.double)
    

    """
    # Used values from code, updated
    p = np.array([0.00549691757211334,
                  4.75686155670860e-10,
                  3.15405721902292e-11,
                  0.00731109320628158,
                  1.40937549145842e-10,
                  0.0876322157772825,
                  0.0256808495553998,
                  5.87110587298283e-11,
                  0.171008417902939,
                  3.80805359553021e-12,
                  9.86953381462523e-12,
                  0.0231020787600445,
                  0.00638922328831119,
                  0.0350184289706385], dtype=np.double)
    """

    # The papers by Kroon as well as the matlab code starts indexing from 1 so
    # do expand array into variables for easier readability
    p01 = p[0]
    p02 = p[1]
    p03 = p[2]
    p04 = p[3]
    p05 = p[4]
    p06 = p[5]
    p07 = p[6]
    p08 = p[7]
    p09 = p[8]
    p10 = p[9]
    p11 = p[10]
    p12 = p[11]
    p13 = p[12]
    p14 = p[13]

    kx = np.array([[-p13, 0, p13],
                   [-p14, 0, p14],
                   [-p13, 0, p13]], dtype=float)

    kxx = np.array([[p01, p04, -p07, p04, p01],
                    [p02, p05, -p08, p05, p02],
                    [p03, p06, -p09, p06, p03],
                    [p02, p05, -p08, p05, p02],
                    [p01, p04, -p07, p04, p01]], dtype=float)

    kxy = np.array([[ p10,  p11, 0, -p11, -p10],
                    [ p11,  p12, 0, -p12, -p11],
                    [   0,    0, 0,    0,    0],
                    [-p11, -p12, 0,  p12,  p11],
                    [-p10, -p11, 0,  p11,  p10]], dtype=float)

    kx = -kx
    ky = kx.transpose()
    return kx, ky, kxx, kxy

# ==============================================================================
# Test function x-ramp
# ==============================================================================

class Differentiable(ABC):

    def _differentiate(self) -> None:
        # Symbolic function expression
        x = self.x
        y = self.y
        f = self.f_sym
        
        # Perform symbolic differentiation
        self.f_x_sym  = f.diff(x)
        self.f_y_sym  = f.diff(y)
        self.f_xx_sym = self.f_x_sym.diff(x)
        self.f_xy_sym = self.f_x_sym.diff(y)
        return

    def _diffString(self) -> str:
        msg = ''
        msg += f'f(x,y):    {str(self.f_sym)}\n'
        msg += f'f(x,y)_x:  {str(self.f_x_sym)}\n'
        msg += f'f(x,y)_y:  {str(self.f_y_sym)}\n'
        msg += f'f(x,y)_xx: {str(self.f_xx_sym)}\n'
        msg += f'f(x,y)_xy: {str(self.f_xy_sym)}\n'
        return msg

    def _numpyString(self) -> str:

        def toNumpyString(expr: str) -> str:
            expr = expr.replace('cos', 'np.cos')
            expr = expr.replace('sin', 'np.sin')
            expr = expr.replace('exp', 'np.exp')
            expr = expr.replace('sqrt', 'np.sqrt')
            expr = expr.replace('x', 'X')
            expr = expr.replace('y', 'Y')
            return expr

        msg = ''
        msg += f'f(x,y):    {toNumpyString(str(self.f_sym))}\n'
        msg += f'f(x,y)_x:  {toNumpyString(str(self.f_x_sym))}\n'
        msg += f'f(x,y)_y:  {toNumpyString(str(self.f_y_sym))}\n'
        msg += f'f(x,y)_xx: {toNumpyString(str(self.f_xx_sym))}\n'
        msg += f'f(x,y)_xy: {toNumpyString(str(self.f_xy_sym))}\n'
        return msg

    @abstractmethod
    def f(self, X, Y) -> np.ndarray:
        pass

    @abstractmethod
    def f_x(self, X, Y) -> np.ndarray:
        pass

    @abstractmethod
    def f_y(self, X, Y) -> np.ndarray:
        pass

    @abstractmethod
    def f_xx(self, X, Y) -> np.ndarray:
        pass

    @abstractmethod
    def f_xy(self, X, Y) -> np.ndarray:
        pass

class OscillatingFunction(Differentiable):
    """
        An oscillating function in 2D, evaluated on a grid
    """

    def __init__(self):

        # Symbolic expression, variables and constants
        x = sympy.Symbol('x'); self.x = x
        y = sympy.Symbol('y'); self.y = y
        
        a = sympy.Symbol('a'); self.a_s = a
        b = sympy.Symbol('b'); self.b_s = b
        c = sympy.Symbol('c'); self.c_s = c
        self.f_sym = a / sympy.sqrt(x**2 + y**2 + b) * sympy.cos( c * (x**2 + y**2) )
        self._differentiate()

        # Constants
        self.a = 2.0
        self.b = 64.0
        self.c = 0.01

    def __str__(self) -> str:
        msg = '## ' + type(self).__name__ + '\n'
        #msg += self._diffString()
        msg += self._numpyString()
        msg += f'a: {self.a}\n'
        msg += f'b: {self.b}\n'
        msg += f'c: {self.c}\n'
        return msg

    def f(self, X, Y) -> np.ndarray:
        a = self.a; b = self.b; c = self.c
        Z = a / np.sqrt(X**2 + Y**2 + b) * np.cos( c * (X**2 + Y**2) )
        return Z

    def f_x(self, X, Y) -> np.ndarray:
        a = self.a; b = self.b; c = self.c
        Z = -2 * a * c * X * np.sin( c * (X**2 + Y**2)) / np.sqrt(b + X**2 + X**2) - a * X * np.cos( c * (X**2 + Y**2))/(b + X**2 + Y**2)**(3.0/2.0)
        return Z

    def f_y(self, X, Y) -> np.ndarray:
        a = self.a; b = self.b; c = self.c
        Z = -2 * a * c * Y * np.sin( c * (X**2 + Y**2)) / np.sqrt(b + X**2 + Y**2) - a * Y * np.cos( c * (X**2 + Y**2))/(b + X**2 + Y**2)**(3.0/2.0)
        return Z

    def f_xx(self, X, Y) -> np.ndarray:
        a = self.a; b = self.b; c = self.c
        Z = -4 * a * c**2 * X**2 * np.cos( c * (X**2 + Y**2)) / np.sqrt(b + X**2 + Y**2) + 4 * a * c * X**2 * np.sin( c * (X**2 + Y**2)) / (b + X**2 + Y**2)**(3.0/2.0) - 2 * a * c * np.sin( c * (X**2 + Y**2)) / np.sqrt(b + X**2 + Y**2) + 3 * a * X**2 * np.cos( c * (X**2 + Y**2)) / (b + X**2 + Y**2)**(5.0/2.0) - a * np.cos( c * (X**2 + Y**2)) / (b + X**2 + Y**2)**(3.0/2.0)
        return Z

    def f_xy(self, X, Y) -> np.ndarray:
        a = self.a; b = self.b; c = self.c
        Z = -4 * a * c**2 * X * Y * np.cos( c * (X**2 + Y**2)) / np.sqrt(b + X**2 + Y**2) + 4 * a * c * X * Y * np.sin( c * (X**2 + Y**2)) / (b + X**2 + Y**2)**(3.0/2.0) + 3 * a * X * Y * np.cos( c * (X**2 + X**2) ) / (b + X**2 + Y**2)**(5.0/2.0)
        return Z

class RampFunctionX(Differentiable):
    """
        A ramp function in 2D, evaluated on a grid
    """

    def __init__(self):

        # Symbolic expression, variables and constants
        x = sympy.Symbol('x'); self.x = x
        y = sympy.Symbol('y'); self.y = y

        k = sympy.Symbol('k'); self.k_s = k
        m = sympy.Symbol('m'); self.m_s = m
        self.f_sym = k*x + m
        self._differentiate()

        # Constants
        self.k = 2
        self.m = 3.5

    def __str__(self) -> str:
        msg = '## ' + type(self).__name__ + '\n'
        #msg += self._diffString()
        msg += self._numpyString()
        msg += f'm: {self.m}\n'
        msg += f'k: {self.k}\n'
        return msg

    def f(self, X, Y) -> np.ndarray:
        k = self.k; m = self.m
        Z = k*X + m
        return Z

    def f_x(self, X, Y) -> np.ndarray:
        k = self.k; m = self.m
        Z = k * np.ones(X.shape)
        return Z

    def f_y(self, X, Y) -> np.ndarray:
        Z = np.zeros(X.shape)
        return Z

    def f_xx(self, X, Y) -> np.ndarray:
        Z = np.zeros(X.shape)
        return Z

    def f_xy(self, X, Y) -> np.ndarray:
        Z = np.zeros(X.shape)
        return Z

class RampFunctionY(Differentiable):
    """
        A ramp function in 2D, evaluated on a grid
    """

    def __init__(self):

        # Symbolic expression, variables and constants
        x = sympy.Symbol('x'); self.x = x
        y = sympy.Symbol('y'); self.y = y

        k = sympy.Symbol('k'); self.k_s = k
        m = sympy.Symbol('m'); self.m_s = m
        self.f_sym = k*y + m
        self._differentiate()

        # Constants
        self.k = 2
        self.m = 3.5

    def __str__(self) -> str:
        msg = '## ' + type(self).__name__ + '\n'
        #msg += self._diffString()
        msg += self._numpyString()
        msg += f'm: {self.m}\n'
        msg += f'k: {self.k}\n'
        return msg

    def f(self, X, Y) -> np.ndarray:
        k = self.k; m = self.m
        Z = k*Y + m
        return Z

    def f_x(self, X, Y) -> np.ndarray:
        k = self.k; m = self.m
        Z = np.zeros(X.shape)
        return Z

    def f_y(self, X, Y) -> np.ndarray:
        k = self.k; m = self.m
        Z = k * np.ones(X.shape)
        return Z

    def f_xx(self, X, Y) -> np.ndarray:
        Z = np.zeros(X.shape)
        return Z

    def f_xy(self, X, Y) -> np.ndarray:
        Z = np.zeros(X.shape)
        return Z

class RampFunctionXY(Differentiable):
    """
        A 2D ramp function in 2D, evaluated on a grid
    """

    def __init__(self):

        # Symbolic expression, variables and constants
        x = sympy.Symbol('x'); self.x = x
        y = sympy.Symbol('y'); self.y = y

        k0 = sympy.Symbol('k0'); self.k0_s = k0
        k1 = sympy.Symbol('k1'); self.k1_s = k1
        m = sympy.Symbol('m'); self.m_s = m
        self.f_sym = k0*x + k1*y + m
        self._differentiate()

        # Constants
        self.k0 = 2
        self.k1 = 3
        self.m = 3.5

    def __str__(self) -> str:
        msg = '## ' + type(self).__name__ + '\n'
        #msg += self._diffString()
        msg += self._numpyString()
        msg += f'm:  {self.m}\n'
        msg += f'k0: {self.k0}\n'
        msg += f'k1: {self.k1}\n'
        return msg

    def f(self, X, Y) -> np.ndarray:
        k0 = self.k0; k1 = self.k1; m = self.m
        Z = k0*X + k1*Y + m
        return Z

    def f_x(self, X, Y) -> np.ndarray:
        k0 = self.k0; k1 = self.k1; m = self.m
        Z = k0 * np.ones(X.shape)
        return Z

    def f_y(self, X, Y) -> np.ndarray:
        k0 = self.k0; k1 = self.k1; m = self.m
        Z = k1 * np.ones(X.shape)
        return Z

    def f_xx(self, X, Y) -> np.ndarray:
        Z = np.zeros(X.shape)
        return Z

    def f_xy(self, X, Y) -> np.ndarray:
        Z = np.zeros(X.shape)
        return Z

class QuadPolFunctionXY(Differentiable):
    """
        A 2D polynomial function in 2D, evaluated on a grid
    """

    def __init__(self):

        # Symbolic expression, variables and constants
        x = sympy.Symbol('x'); self.x = x
        y = sympy.Symbol('y'); self.y = y

        k0 = sympy.Symbol('k0'); self.k0_s = k0
        k1 = sympy.Symbol('k1'); self.k1_s = k1
        k2 = sympy.Symbol('k2'); self.k2_s = k2
        k3 = sympy.Symbol('k3'); self.k3_s = k3
        k4 = sympy.Symbol('k4'); self.k4_s = k4
        m = sympy.Symbol('m'); self.m_s = m
        self.f_sym = k0*x**2 + k1*y**2 + k2*x + k3*y + k4*x*y + m
        self._differentiate()

        # Constants
        self.k0 = -5
        self.k1 =  3
        self.k2 =  2
        self.k3 = -3
        self.k4 = -4
        self.m = 3.5

    def __str__(self) -> str:
        msg = '## ' + type(self).__name__ + '\n'
        #msg += self._diffString()
        msg += self._numpyString()
        msg += f'm:  {self.m}\n'
        msg += f'k0: {self.k0}\n'
        msg += f'k1: {self.k1}\n'
        msg += f'k2: {self.k2}\n'
        msg += f'k3: {self.k3}\n'
        msg += f'k4: {self.k4}\n'
        return msg

    def f(self, X, Y) -> np.ndarray:
        k0 = self.k0; k1 = self.k1; k2 = self.k2; k3 = self.k3; k4 = self.k4; m = self.m
        Z = k0*X**2 + k1*Y**2 + k2*X + k3*Y + k4*X*Y + m
        return Z

    def f_x(self, X, Y) -> np.ndarray:
        k0 = self.k0; k1 = self.k1; k2 = self.k2; k3 = self.k3; k4 = self.k4; m = self.m
        Z = 2*k0*X + k2 + k4*Y
        return Z

    def f_y(self, X, Y) -> np.ndarray:
        k0 = self.k0; k1 = self.k1; k2 = self.k2; k3 = self.k3; k4 = self.k4; m = self.m
        Z = 2*k1*Y + k3 + k4*X
        return Z

    def f_xx(self, X, Y) -> np.ndarray:
        k0 = self.k0; k1 = self.k1; k2 = self.k2; k3 = self.k3; k4 = self.k4; m = self.m
        Z = 2*k0 * np.ones(X.shape)
        return Z

    def f_xy(self, X, Y) -> np.ndarray:
        k0 = self.k0; k1 = self.k1; k2 = self.k2; k3 = self.k3; k4 = self.k4; m = self.m
        Z = k4 * np.ones(X.shape)
        return Z

# ==============================================================================
# Numerical differentiation comparisons
# ==============================================================================

def plotDerivativeResults(
    test_function: Differentiable,
    plot_results: Dict[str, bool],
    N: int,
    h: float
):

    def genGrid(
        N: int,
        delta: float,
    ):
        """
            Generate the X, Y grid for 2D and 3D plots
        """
        x = y = np.arange(-N*np.pi, N*np.pi, delta)
        X, Y = np.meshgrid(x, y)
        return X, Y, x, y

    X, Y, x, y = genGrid(N, h)
    extent = [x[0], x[-1], y[0], y[-1]]

    Z   = test_function.f(X, Y)
    Zx  = test_function.f_x(X, Y)
    Zy  = test_function.f_y(X, Y)
    Zxx = test_function.f_xx(X, Y)
    Zxy = test_function.f_xy(X, Y)
    Zang = np.arctan2(Zy, Zx) / np.pi * 180.0

    fz_arr = {}
    fz_arr['f_x']   = [Zx]
    fz_arr['f_y']   = [Zy]
    fz_arr['f_ang'] = [Zang]
    fz_arr['f_xx']  = [Zxx]
    fz_arr['f_xy']  = [Zxy]

    kernel_functions = [getFiniteDifferenceKernels, getSobelKernels, getScharrKernels, getKroonKernels]
    for i, kfun in enumerate(kernel_functions):
        kx, ky, kxx, kxy = kfun()
        # The scaling with h is not accurate for the Kroon kernels...
        Zx_k   = scipy.signal.convolve2d(Z, kx,  mode='same', boundary='symm') * (1.0/h)
        Zy_k   = scipy.signal.convolve2d(Z, ky,  mode='same', boundary='symm') * (1.0/h)
        Zxx_k  = scipy.signal.convolve2d(Z, kxx, mode='same', boundary='symm') * (1.0/(h*h))
        Zxy_k  = scipy.signal.convolve2d(Z, kxy, mode='same', boundary='symm') * (1.0/(h*h))
        Zang_k = np.arctan2(Zy_k, Zx_k) / np.pi * 180.0

        print(kx)

        fz_arr['f_x'].append(Zx_k)
        fz_arr['f_y'].append(Zy_k)
        fz_arr['f_ang'].append(Zang_k)
        fz_arr['f_xx'].append(Zxx_k)
        fz_arr['f_xy'].append(Zxy_k)

    # Compare with explicit functions
    explicit_functions = [computeSobel, computeScharr, computeKroon]
    for i in range(2,len(fz_arr['f_x'])):
        Zx_k  = fz_arr['f_x'][i]
        Zy_k  = fz_arr['f_y'][i]
        Zxx_k = fz_arr['f_xx'][i]
        Zxy_k = fz_arr['f_xy'][i]

        Zx_e, Zy_e, Zxx_e, Zxy_e = explicit_functions[i-2](Z)

        eps = 1e-6
        Zx_same  = (np.abs( Zx_k[5:-5,5:-5] -  Zx_e[5:-5,5:-5]) < eps).all()
        Zy_same  = (np.abs( Zy_k[5:-5,5:-5] -  Zy_e[5:-5,5:-5]) < eps).all()
        Zxx_same = (np.abs(Zxx_k[5:-5,5:-5] - Zxx_e[5:-5,5:-5]) < eps).all()
        Zxy_same = (np.abs(Zxy_k[5:-5,5:-5] - Zxy_e[5:-5,5:-5]) < eps).all()
        print(f"Zx_same:  {Zx_same}")
        print(f"Zy_same:  {Zy_same}")
        print(f"Zxx_same: {Zxx_same}")
        print(f"Zxy_same: {Zxy_same}")

    #LatexPreamble.latexMatplotlibPreample()
    print(f"Sample grid size: {X.shape[0]}x{X.shape[1]}")

    cm_div = plt.cm.bwr
    cm_div = plt.cm.magma
    cm_seq = plt.cm.viridis
    my_dpi=96

    fig = plt.figure(figsize=(6,6), dpi=my_dpi)
    fig.suptitle(type(test_function).__name__)
    ax = fig.gca()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    CS = ax.imshow(Z, origin='lower', cmap=cm_seq, extent=extent)
    plt.show()
    plt.close()

    titles = ['Analytic', 'FD', 'Sobel', 'Scharr', 'Kroon']
    for key in list(fz_arr.keys()):
        fig = plt.figure(figsize=(5*6, 7), dpi=my_dpi)
        gs = fig.add_gridspec(2, 5, hspace=0.1, wspace=0.1)
        axs = gs.subplots(sharex=True, sharey=True)
        fig.suptitle(key)

        val_arr = fz_arr[key]

        val_min = np.zeros( (1, len(val_arr)) ).flatten()
        val_max = np.zeros( (1, len(val_arr)) ).flatten()
        for i in range(len(val_arr)):
            val_min[i] = np.amin(val_arr[i])
            val_max[i] = np.amax(val_arr[i])
        vmin = np.amin(val_min)
        vmax = np.amax(val_max)

        diff_min = np.zeros( (1, len(val_arr)) ).flatten()
        diff_max = np.zeros( (1, len(val_arr)) ).flatten()
        diff_avg = np.zeros( (1, len(val_arr)) ).flatten()
        for i in range(len(val_arr)):
            diff_min[i] = np.amin(np.abs(val_arr[i]-val_arr[0]))
            diff_max[i] = np.amax(np.abs(val_arr[i]-val_arr[0]))
            diff_avg[i] = np.mean(np.abs(val_arr[i]-val_arr[0]))
        dmin = np.amin(diff_min)
        dmax = np.amax(diff_max)
        print("Diff Min: ", end=""); print(dmin)
        print("Diff Max: ", end=""); print(dmax)

        for i in range(len(val_arr)):
            ax: plt.Axes = axs[0,i]
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            CS = ax.imshow(val_arr[i], origin='lower', cmap=cm_seq, extent=extent, vmin=vmin, vmax=vmax)
            ax.set_title(titles[i])

            ax: plt.Axes = axs[1,i]
            #ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            CS = ax.imshow( np.abs(val_arr[i]-val_arr[0]), origin='lower', cmap=cm_div, extent=extent, vmin=dmin, vmax=dmax)
            ax.set_xlabel(f'Min diff: {diff_min[i]:.8f}\nMax diff: {diff_max[i]:.8f}\nAvg diff: {diff_avg[i]:.8f}')

        if plot_results[key]:
            plt.show()
        plt.close()

        print("Min: ", end=""); print(val_min)
        print("Max: ", end=""); print(val_max)
    return

# ==============================================================================
# Main
# ==============================================================================

if __name__ == '__main__':

    ramp_f0 = RampFunctionX()
    ramp_f1 = RampFunctionY()
    ramp_f2 = RampFunctionXY()
    quad_f = QuadPolFunctionXY()
    osc_f = OscillatingFunction()

    plot_results = {'f_x': True, 'f_y': True, 'f_ang': True, 'f_xx': True, 'f_xy': True}
    #plot_results = {'f_x': False, 'f_y': False, 'f_ang': False, 'f_xx': False, 'f_xy': False}

    print(ramp_f0)
    print(ramp_f1)
    print(quad_f)
    print(osc_f)

    #plot_results = {'f_x': True, 'f_y': False, 'f_ang': False, 'f_xx': False, 'f_xy': False}
    #plotDerivativeResults(ramp_f0, plot_results, 20, 1)
    #plot_results = {'f_x': False, 'f_y': False, 'f_ang': False, 'f_xx': False, 'f_xy': False}
    #plotDerivativeResults(ramp_f1, plot_results, 20, 1)
    #plotDerivativeResults(ramp_f2, plot_results, 20, 1)
    #plotDerivativeResults(quad_f, plot_results, 20, 1)
    plotDerivativeResults(osc_f, plot_results, 20, 1.0)
