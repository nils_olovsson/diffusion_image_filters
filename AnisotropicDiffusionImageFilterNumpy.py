"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

import os
import curses
#from curses import _CursesWindow

from typing import Tuple, List, Dict

import numpy as np
from numpy.core.umath import multiply
import scipy.signal

import skimage.color

import DifferentialKernels

def rgb2gray(img):
    ndim = len(img.shape)
    gr8 = img
    if ndim == 3:
        if img.shape[2]==3:
            gr8 = skimage.color.rgb2gray(img)
        elif img.shape[2]==4:
            gr8 = skimage.color.rgb2gray(skimage.color.rgba2rgb(img))
    return gr8

def anisotropicDiffusionFilterNumpy(
    image: np.ndarray,
    iterations = 10,
    sigma = 1.5,
    rho   = 1.5,
    alpha = 0.1,
    mode  = 1,
    tau   = 0.125,
    image_seq = None,
    kernel_type = 2,
    zero_div    = 0,
    curses_win = None,
) -> np.ndarray:
    """
        Execute anisotropic smoothing filter on image.
        The method is described in the 1998 monograph by Weickert.
        mode=0 : Edge enhancing
        mode=1 : Coherence enhancing
        mode=2 : Directed smearing (along)
        mode=3 : Directed smearing (across)
    """

    def computeUpdateNumpy(
        u: np.ndarray,
        D: np.ndarray,
        kernels: Dict[str, np.ndarray],
        zero_div=0,
    ):
        """
            Comput the update for the next iteration using the spatial
            derivatives.
                kernal_type: 0 - Ordinary finite differences
                             1 - Sobel
                             2 - Scharr
                             3 - Kroon
                zero_div: 0 - Use both divergence and trace terms
                          1 - Only use trace term (100% smooth diffusivity)
        """
        if len(u.shape) == 2:
            update = computeUpdateAnisotropicNumpyKernel1Ch(u, D, kernels, zero_div)
        else:
            update = computeUpdateAnisotropicNumpyKernelNCh(u, D, kernels, zero_div)
        return update

    def computeUpdateAnisotropicNumpyKernel1Ch(
        u: np.ndarray,
        D: np.ndarray,
        kernels: Dict[str, np.ndarray],
        zero_div: int,
    ):
        """
            Compute the update for the next iteration using the spatial derivatives.
        """

        a_chan = 0
        b_chan = 1
        c_chan = 2
        d_chan = 3

        a = D[a_chan, :, :]
        b = D[b_chan, :, :]
        c = D[c_chan, :, :]
        d = D[d_chan, :, :]

        kx  = kernels['kx']
        ky  = kernels['ky']
        kxx = kernels['kxx']
        kyy = kernels['kyy']
        kxy = kernels['kxy']

        a_x = scipy.signal.convolve2d(a, kx, mode='same', boundary='symm')
        b_y = scipy.signal.convolve2d(b, ky, mode='same', boundary='symm')
        c_x = scipy.signal.convolve2d(c, kx, mode='same', boundary='symm')
        d_y = scipy.signal.convolve2d(d, ky, mode='same', boundary='symm')

        u_x = scipy.signal.convolve2d(u, kx, mode='same', boundary='symm')
        u_y = scipy.signal.convolve2d(u, ky, mode='same', boundary='symm')

        u_xx = scipy.signal.convolve2d(u, kxx, mode='same', boundary='symm')
        u_yy = scipy.signal.convolve2d(u, kyy, mode='same', boundary='symm')
        u_xy = scipy.signal.convolve2d(u, kxy, mode='same', boundary='symm')

        divDnablau     = np.multiply((a_x + b_y), u_x) + np.multiply((c_x + d_y), u_y)
        trDnablanablau = np.multiply(a, u_xx) + np.multiply(d, u_yy) + np.multiply((b + d), u_xy)

        if zero_div == 0:
            update = divDnablau + trDnablanablau
        else:
            update = trDnablanablau
        return update

    def computeUpdateAnisotropicNumpyKernelNCh(
        u: np.ndarray,
        D: np.ndarray,
        kernels: Dict[str, np.ndarray],
        zero_div: int,
    ):
        """
            Compute the update per channel for the next iteration using the spatial derivatives.
        """
        update = np.zeros(u.shape, dtype=u.dtype)
        for channel in range(u.shape[2]):
            update[:,:,channel] = computeUpdateAnisotropicNumpyKernel1Ch(u[:,:,channel], D, kernels, zero_div)
        return update

    def computeDiffusivityNumpy(
        u: np.ndarray,
        sigma: float,
        rho: float,
        alpha: float,
        kernels: Dict[str, np.ndarray],
    ):
        """
            Compute the anisotropic, matrix valued, diffusivity.
        """
        def computeImageStructureTensorsNumpy(
            grad_x: np.ndarray,
            grad_y: np.ndarray,
        ) -> np.ndarray:
            """
                Compute the matrix valued image structure for anisotropic diffusion
                image filter.
            """
            J = np.zeros( (4, grad_x.shape[0], grad_x.shape[1]), dtype=grad_x.dtype )
            J[0, :, :] = np.multiply(grad_x, grad_x)
            J[1, :, :] = np.multiply(grad_x, grad_y)
            J[2, :, :] = np.multiply(grad_x, grad_y)
            J[3, :, :] = np.multiply(grad_y, grad_y)
            return J

        def computeAnisotropicDiffusivityNumpy(
            J: np.ndarray,
            grad_magnitude: np.ndarray,
            alpha: float,
            mode: int,
        ) -> np.ndarray:
            """
                Hello
            """
            shape = grad_magnitude.shape
            C = 3.315
            #C = 1e-3
            lamb = 4

            j11 = J[0,:,:]
            j12 = J[1,:,:]
            j22 = J[3,:,:]

            j11_j22 = j11 - j22
            sqrt_term = np.sqrt(j11_j22 * j11_j22 + 4.0 * j12 * j12)

            v1a = 2.0 * j12
            v1b = j22 - j11 + sqrt_term

            vnorm = np.sqrt( np.multiply(v1a,v1a) + np.multiply(v1b,v1b) )
            #if vnorm < 1.e-8:
            #    dm = grad_magnitude
            #else:
            #    v1a /= vnorm
            #    v1b /= vnorm
            indices = vnorm >= 1.e-8
            v1a[indices] = np.divide(v1a[indices], vnorm[indices])
            v1b[indices] = np.divide(v1b[indices], vnorm[indices])

            v2a =  v1b
            v2b = -v1a

            m1 = 0.5 * (j11 + j22 + sqrt_term)
            m2 = 0.5 * (j11 + j22 - sqrt_term)

            if mode == 0:
                # Edge enhancing
                l2 = 1.0 * np.ones(shape)
                l1 = 1.0 * np.ones(shape)
                indices = np.abs(m1) > 1.e-10
                l1_tmp = 1.0 - np.exp(-C/ (np.power(m1, lamb) + 1.e-8))
                l1[indices] = l1_tmp[indices]
            elif mode == 1:
                # Coherence enhancing
                l1 = alpha * np.ones(shape)
                l2 = alpha * np.ones(shape)
                dm = np.abs(m1 - m2)
                indices = dm > 1.e-10
                l2_tmp = alpha + (1.0 - alpha) * np.exp( -1.0 / (np.multiply(dm, dm) + 1e-8) )
                l2[indices] = l2_tmp[indices]
            elif mode == 2:
                # Directed diffusion, smear along
                l1 = np.zeros(shape)
                l2 = 0.9 * np.ones(shape)
            elif mode == 3:
                # Directed diffusion, smear across
                l1 = 0.9 * np.ones(shape)
                l2 = np.zeros(shape)
            else:
                raise Exception(f"error: computeAnisotropicDiffusivity()\n    mode not set to valid number (0-2), mode={mode}.")

            # Store the anisotropic diffusivity in array
            a  = np.multiply(l1, np.multiply(v1a, v1a)) + np.multiply(l2, np.multiply(v2a, v2a))
            bc = np.multiply(l1, np.multiply(v1a, v1b)) + np.multiply(l2, np.multiply(v2a, v2b))
            d  = np.multiply(l1, np.multiply(v1b, v1b)) + np.multiply(l2, np.multiply(v2b, v2b))

            D = np.zeros(J.shape)
            D[0,:,:] = a
            D[1,:,:] = bc
            D[2,:,:] = bc
            D[3,:,:] = d

            return D
            
        usig = np.copy(u)
        if sigma>0.0:
            usig = scipy.ndimage.gaussian_filter(u, sigma=sigma)

        kx, ky, _, _, _ = DifferentialKernels.getScharrKernels()
        #kx = kernels['kx']
        #ky = kernels['ky']

        grad_x  = scipy.signal.convolve2d(usig, kx, mode='same', boundary='symm')
        grad_y  = scipy.signal.convolve2d(usig, ky, mode='same', boundary='symm')
        gradm2 = np.power(grad_x, 2) + np.power(grad_y, 2)

        J = computeImageStructureTensorsNumpy(grad_x, grad_y)
        Jrho = np.copy(J)
        if rho > 0.0:
            Jrho[0,:,:] = scipy.ndimage.gaussian_filter(J[0,:,:], sigma=rho)
            Jrho[1,:,:] = scipy.ndimage.gaussian_filter(J[1,:,:], sigma=rho)
            Jrho[2,:,:] = scipy.ndimage.gaussian_filter(J[2,:,:], sigma=rho)
            Jrho[3,:,:] = scipy.ndimage.gaussian_filter(J[3,:,:], sigma=rho)
        D = computeAnisotropicDiffusivityNumpy(Jrho, gradm2, alpha, mode)
        return D

    u = np.copy(image)
    shape = u.shape

    if len(u.shape) > 2 and u.shape[2] == 1:
        u = np.reshape(u, (u.shape[0], u.shape[1]))
    if image_seq != None:
        image_seq.append(np.copy(u))

    if curses_win:
        curses_win.addstr(2, 0, 78*" ")
        curses_win.refresh()

    # Differential ernels are used to calculate derivative approximations
    # in the update function
    kx=ky=kxx=kyy=kxy = None
    if kernel_type == 0:
        kx, ky, kxx, kyy, kxy = DifferentialKernels.getFiniteDifferenceKernels()
    elif kernel_type == 1:
        kx, ky, kxx, kyy, kxy = DifferentialKernels.getSobelKernels()
    elif kernel_type == 2:
        kx, ky, kxx, kyy, kxy = DifferentialKernels.getScharrKernels()
    elif kernel_type == 3:
        kx, ky, kxx, kyy, kxy = DifferentialKernels.getKroonKernels(2)
    kernels = {}
    kernels['kx']  = kx
    kernels['ky']  = ky
    kernels['kxx'] = kxx
    kernels['kyy'] = kyy
    kernels['kxy'] = kxy

    # Scharr kernels are used to calculate gradient for diffusivity
    # for all methods
    kx, ky, kxx, kyy, kxy = DifferentialKernels.getScharrKernels()
    kernels_scharr = {}
    kernels_scharr['kx']  = kx
    kernels_scharr['ky']  = ky
    kernels_scharr['kxx'] = kxx
    kernels_scharr['kyy'] = kyy
    kernels_scharr['kxy'] = kxy

    for i in range(iterations):
        msg = f"Iteration: {i+1}/{iterations}"
        if curses_win:
            curses_win.addstr(2, 0, msg)
            curses_win.refresh()
        else:
            print(msg)

        D = None
        if len(u.shape) == 2:
            D = computeDiffusivityNumpy(u, sigma, rho, alpha, kernels_scharr)
        else:
            g = rgb2gray(u)
            D = computeDiffusivityNumpy(g, sigma, rho, alpha, kernels_scharr)
        update = computeUpdateNumpy(u, D, kernels, zero_div)
        u = u + tau * update
        if image_seq != None:
            image_seq.append(np.copy(u))
    return u
