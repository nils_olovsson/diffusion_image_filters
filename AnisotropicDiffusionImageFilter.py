#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

import os
import curses
#from curses import _CursesWindow

from typing import Tuple, List

import math

import numpy as np
import scipy.signal

import skimage.color

from DiffusionImageFilter import DiffusionImageFilterUpdate

def rgb2gray(img):
    ndim = len(img.shape)
    gr8 = img
    if ndim == 3:
        if img.shape[2]==3:
            gr8 = skimage.color.rgb2gray(img)
        elif img.shape[2]==4:
            gr8 = skimage.color.rgb2gray(skimage.color.rgba2rgb(img))
    return gr8

def anisotropicDiffusionFilter(
    image: np.ndarray,
    iterations = 10,
    sigma = 1.5,
    rho   = 1.5,
    alpha = 0.1,
    mode  = 1,
    tau   = 0.125,
    image_seq = None,
    kernel_type = 2,
    zero_div    = 0,
    curses_win = None,
):
    """
        Execute anisotropic smoothing filter on image.
        The method is described in the 1998 monograph by Weickert.
        mode=0 : Edge enhancing
        mode=1 : Coherence enhancing
        mode=2 : Directed smearing (along)
        mode=3 : Directed smearing (across)
    """

    def computeUpdate(
        u: np.ndarray,
        D: np.ndarray,
        Dpad: np.ndarray,
        kernel_type,
        zero_div=0,
    ):
        """
            Comput the update for the next iteration using the spatial
            derivatives.
                kernal_type: 0 - Ordinary finite differences
                             1 - Sobel
                             2 - Scharr
                             3 - Kroon
                zero_div: 0 - Use both divergence and trace terms
                          1 - Only use trace term (100% smooth diffusivity)
        """
        pad = 2
        if kernel_type == 0:
            pad = 1

        update = np.zeros(u.shape, dtype=float)
        if len(u.shape) == 2:
            u = np.pad(u, pad_width=pad, mode='constant')
        else:
            upad = np.zeros((u.shape[0]+2*pad, u.shape[1]+2*pad, u.shape[2]), dtype=u.dtype)
            for channel in range(u.shape[2]):
                upad[:,:,channel] = np.pad(u[:,:,channel], pad_width=pad, mode='constant')
            u = upad

        #print("Pad!")
        Dpad[0,:,:] = np.pad(D[0,:,:], pad_width=pad, mode='constant')
        Dpad[1,:,:] = np.pad(D[1,:,:], pad_width=pad, mode='constant')
        Dpad[2,:,:] = np.pad(D[2,:,:], pad_width=pad, mode='constant')
        Dpad[3,:,:] = np.pad(D[3,:,:], pad_width=pad, mode='constant')
        #print("Padded!")

        if len(u.shape) == 2:
            return DiffusionImageFilterUpdate.computeUpdateAnisotropicKernel1Ch(u, update, Dpad, kernel_type, zero_div)
        else:
            return DiffusionImageFilterUpdate.computeUpdateAnisotropicKernelNCh(u, update, Dpad, kernel_type, zero_div)
        #return u[1:-1,1:-1]

    def computeDiffusivity(
        u: np.ndarray,
        sigma: float,
        rho: float,
        alpha: float,
        mode: int,
        J: np.ndarray,
        Jrho: np.ndarray,
        D: np.ndarray,
    ):
        """
            Compute the anisotropic, matrix valued, diffusivity.
        """

        shape = u.shape
        if len(shape) > 2 and shape[2] > 1:
            print("RGB to gray")
            u = skimage.color.rgb2gray(u)
        usig = np.copy(u)
        if sigma>0.0:
            usig = scipy.ndimage.gaussian_filter(u, sigma=sigma)

        #kx = 0.5 * np.array([[ 0.0, 0.0, 0.0],
        #                              [-1.0, 0.0, 1.0],
        #                              [ 0.0, 0.0, 0.0]])
        #ky = 0.5 * np.array([[ 0.0,-1.0, 0.0],
        #                              [ 0.0, 0.0, 0.0],
        #                              [ 0.0, 1.0, 0.0]])
        """
        v0 = 47
        v1 = 162

        kx = np.array([[v0, 0.0, -v0],
                       [v1, 0.0, -v1],
                       [v0, 0.0, -v0]])
        ky = np.array([[ v0,   v1,   v0],
                       [ 0.0, 0.0,  0.0],
                       [-v0,  -v1, -v0]])
        kx /= 512.0
        ky /= 512.0
        """

        #grad_x  = scipy.signal.convolve2d(usig, kx, boundary='symm')
        #grad_y  = scipy.signal.convolve2d(usig, ky, boundary='symm')
        #gradm2 = np.power(grad_x, 2) + np.power(grad_y, 2)

        #grad_x = grad_x[1:-1, 1:-1]
        #grad_y = grad_y[1:-1, 1:-1]
        #gradm2 = gradm2[1:-1, 1:-1]

        grad_x  = np.zeros(u.shape, np.float)
        grad_y  = np.zeros(u.shape, np.float)
        DiffusionImageFilterUpdate.gradU(usig, grad_x, grad_y, 0)
        gradm2 = np.power(grad_x, 2) + np.power(grad_y, 2)

        #print(u.shape)
        #print(usig.shape)
        #print(grad_x.shape)
        #print(grad_y.shape)
        #input("Ok...")
        #J    = np.zeros((4, shape[0], shape[1]), dtype=np.float64)
        #Jrho = np.zeros((4, shape[0], shape[1]), dtype=np.float64)
        #D    = np.zeros((4, shape[0], shape[1]), dtype=np.float64)

        #print("Compute the structure tensor")
        J = DiffusionImageFilterUpdate.computeImageStructureTensors(grad_x, grad_y, J)
        Jrho = np.copy(J)
        if rho > 0.0:
            Jrho[0,:,:] = scipy.ndimage.gaussian_filter(J[0,:,:], sigma=rho)
            Jrho[1,:,:] = scipy.ndimage.gaussian_filter(J[1,:,:], sigma=rho)
            Jrho[2,:,:] = scipy.ndimage.gaussian_filter(J[2,:,:], sigma=rho)
            Jrho[3,:,:] = scipy.ndimage.gaussian_filter(J[3,:,:], sigma=rho)
        D = DiffusionImageFilterUpdate.computeAnisotropicDiffusivity(Jrho, gradm2, D, alpha, mode)
        return D

    u = np.copy(image)
    shape = u.shape
    J    = np.zeros((4, shape[0], shape[1]), dtype=np.float64)
    Jrho = np.zeros((4, shape[0], shape[1]), dtype=np.float64)
    D    = np.zeros((4, shape[0], shape[1]), dtype=np.float64)

    pad = 2
    if kernel_type == 0:
        pad = 1
    Dpad = np.zeros((4, shape[0]+2*pad, shape[1]+2*pad), dtype=np.float64)
    #Dpad = np.zeros((4, shape[0]+2, shape[1]+2), dtype=np.float64)

    if len(u.shape) > 2 and u.shape[2] == 1:
        u = np.reshape(u, (u.shape[0], u.shape[1]))
    if image_seq != None:
        image_seq.append(np.copy(u))

    if curses_win:
        curses_win.addstr(2, 0, 78*" ")
        curses_win.refresh()

    for i in range(iterations):
        msg = f"Iteration: {i+1}/{iterations}"
        if curses_win:
            curses_win.addstr(2, 0, msg)
            curses_win.refresh()
        else:
            print(msg)

        if len(u.shape) == 2:
            D = computeDiffusivity(u, sigma, rho, alpha, mode, J, Jrho, D)
            update = computeUpdate(u, D, Dpad, kernel_type, zero_div)
        else:
            g = rgb2gray(u)
            D = computeDiffusivity(g, sigma, rho, alpha, mode, J, Jrho, D)
            update = computeUpdate(u, D, Dpad, kernel_type, zero_div)
        u = u + tau * update
        if image_seq != None:
            image_seq.append(np.copy(u))
    return u

