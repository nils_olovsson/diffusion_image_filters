#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

import os
import sys

import shutil
from pathlib import Path

from typing import Tuple, List

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

import pydicom
import skimage.io
import skimage.exposure
import skimage.transform

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import FileSystem
import LatexPreamble

import Image2DNoise

from NonlinearDiffusionImageFilter import computeGradMagnAndDiffusivity
from NonlinearDiffusionImageFilter import nonlinearDiffusionFilter
from NonlinearDiffusionImageFilter import linearDiffusionFilter

def getPilImageFont(fontsize=12.0):
    #fontpath = os.path.join('.', 'fonts', 'CharisSIL', 'CharisSIL-Regular.ttf')
    fontpath = os.path.join('.', 'fonts', 'ttf', 'STIXGeneral.ttf')
    font = ImageFont.truetype(fontpath, fontsize)
    return font

# ==============================================================================
# CT image test with nonlinear and linear diffusion comparision video
# ==============================================================================

def gray2gray3ch(img):
    w, h = img.shape
    trg = np.zeros((w,h,3), dtype=img.dtype)
    trg[:,:,0] = img
    trg[:,:,1] = img
    trg[:,:,2] = img
    return trg

def addTextToCtExample(img, iteration):

    offset = int(0.3 * 512)
    stride = 512

    pil_image = Image.fromarray(img)
    draw = ImageDraw.Draw(pil_image)
    font = getPilImageFont(58)
    draw.text((np.round(offset + 0*stride),    np.round(stride-95)), "Original",  (255,255,255), font=font, align='center')
    draw.text((np.round(offset + 1*stride),    np.round(stride-95)), "Nonlinear", (255,255,255), font=font, align='center')
    draw.text((np.round(offset + 2*stride+30), np.round(stride-95)), "Linear",    (255,255,255), font=font, align='center')

    font = getPilImageFont(48)
    draw.text((np.round(offset + 1*stride+20), np.round(1*stride - 10)), f"Iteration {iteration+1}", (0,0,0), font=font, anchor='ms')
    draw.text((np.round(offset + 2*stride),    np.round(1*stride - 10)), f"Iteration {iteration+1}", (0,0,0), font=font, anchor='ms')

    font = getPilImageFont(58)
    draw.text((5, -20),                     "CT values", (255,255,255), font=font)
    draw.text((5, np.round(2*stride - 95)), "Colored using tissue labeling", (0,0,0), font=font)

    return np.asarray(pil_image)

def addTextToCtExample3x(img, color = (0, 0, 0), text = []):

    if not text:
        text = []
        text.append('Original')
        text.append('Nonlinear\nsmoothing')
        text.append('Linear\nsmoothing')

    offset = int(0.3 * 512)
    stride = 512

    pil_image = Image.fromarray(img)
    draw = ImageDraw.Draw(pil_image)
    font = getPilImageFont(58)
    draw.text((offset + 0*stride, stride-110), text[0], color, font=font, anchor='ms', align='center')
    draw.text((offset + 1*stride, stride-110), text[1], color, font=font, anchor='ms', align='center')
    draw.text((offset + 2*stride, stride-110), text[2], color, font=font, anchor='ms', align='center')

    img = np.asarray(pil_image)
    img = img[50:,:,:]
    return img

def concatImages(img00, img01, img02, img10, img11, img12):
    shape = img00.shape
    w = shape[0]
    h = shape[1]
    #print(shape)
    #print(f"w: {w} h: {h}")
    img = np.zeros((2*h, 3*w, 3), dtype=img00.dtype)
    img[0:h,   0:  w, 0] = img00
    img[0:h,   w:2*w, 0] = img01
    img[0:h, 2*w:3*w, 0] = img02
    img[0:h,   0:  w, 1] = img00
    img[0:h,   w:2*w, 1] = img01
    img[0:h, 2*w:3*w, 1] = img02
    img[0:h,   0:  w, 2] = img00
    img[0:h,   w:2*w, 2] = img01
    img[0:h, 2*w:3*w, 2] = img02

    img[h:2*h,   0:  w, :] = img10
    img[h:2*h,   w:2*w, :] = img11
    img[h:2*h, 2*w:3*w, :] = img12

    shape = img.shape
    #print(shape)
    return img

def concatImages3x(img0, img1, img2):
    shape = img0.shape
    w = shape[0]
    h = shape[1]
    c = 1
    if len(shape)>2:
        c = shape[2]
    img = 255 * np.ones((int(np.round(1.10 * h)), 3*w, c), dtype=img0.dtype)

    if c>1:
        img[0:h,   0:  w, :] = img0
        img[0:h,   w:2*w, :] = img1
        img[0:h, 2*w:3*w, :] = img2
    else:
        img[0:h,   0:  w, 0] = img0
        img[0:h,   w:2*w, 0] = img1
        img[0:h, 2*w:3*w, 0] = img2

    shape = img.shape
    print(shape)
    return img

def makeCtColorTable(img, ctdata, mask):
    min_value = np.amin(img)
    #offset = 1000.0
    offset = 0.0

    ctable = (len(ctdata)-1)*[None]
    colors = (len(ctdata)-1)*[None]
    ctable[ 0] = min_value;           colors[ 0] = np.array([0.0, 0.0, 0.0])
    ctable[ 1] = offset + ctdata[ 1]; colors[ 1] = np.array([0.0, 0.0, 0.5]);
    ctable[ 2] = offset + ctdata[ 2]; colors[ 2] = np.array([255.0/255.0, 255.0/255.0, 224.0/255.0])
    ctable[ 3] = offset + ctdata[ 3]; colors[ 3] = np.array([255.0/255.0, 228.0/255.0, 181.0/255.0])
    ctable[ 4] = offset + ctdata[ 4]; colors[ 4] = np.array([255.0/255.0, 192.0/255.0, 203.0/255.0])
    ctable[ 5] = offset + ctdata[ 6]; colors[ 5] = np.array([255.0/255.0, 0.0, 0.0])
    ctable[ 6] = offset + ctdata[ 7]; colors[ 6] = np.array([250.0/255.0, 128.0/255.0, 114.0/255.0])
    ctable[ 7] = offset + ctdata[ 8]; colors[ 7] = np.array([0.5, 0.0, 0.0]);
    ctable[ 8] = offset + ctdata[ 9]; colors[ 8] = np.array([0.75, 0.40, 0.50])
    ctable[ 9] = offset + ctdata[10]; colors[ 9] = np.array([0.75, 0.60, 0.60])
    ctable[10] = offset + ctdata[11]; colors[10] = np.array([0.90, 0.90, 0.90])
    ctable[11] = offset + ctdata[12]; colors[11] = np.array([0.95, 0.95, 0.95])
    ctable[12] = ctable[11] + 100.0;  colors[12] = np.array([1.00, 1.00, 1.00])

    r = np.zeros(img.shape)
    g = np.zeros(img.shape)
    b = np.zeros(img.shape)

    for i in range(0,len(ctable)): #enumerate(ctable):
        v = ctable[i]
        ind = img >= v
        r[ind] = colors[i][0]
        g[ind] = colors[i][1]
        b[ind] = colors[i][2]

    r[mask==0] = 1.0
    g[mask==0] = 1.0
    b[mask==0] = 1.0

    colorimg = np.dstack((r,g,b))
    colorimg = (255.0 * colorimg).astype(np.uint8)
    return colorimg

def gaussFilter(img, sigma):
    gkernel = 0.5 * np.array([[ 0.0,-1.0, 0.0],
                              [ 0.0, 0.0, 0.0],
                              [ 0.0, 1.0, 0.0]])
    res = scipy.signal.convolve2d(img, gkernel, boundary='symm')

def makeCtExampleImages(make_images=True, make_video=True):
    print("Making CT example images...")

    ct = pd.read_csv('data/ct-tissue-values.csv')
    print(ct)
    ct = ct['Philips'].tolist()
    #ct = ct['Siemens'].tolist()
    #ct = ct['Toshiba'].tolist()

    filepath = os.path.join('.', 'data', 'image_72.dcm')
    dcm = pydicom.dcmread(filepath)
    filepath = os.path.join('.', 'data', 'mask.png')
    mask = skimage.io.imread(filepath)
    mask = skimage.color.rgb2gray(mask)

    f = open("dicom-metadata.txt", "w")
    f.write(str(dcm))
    f.close()

    org = dcm.pixel_array.astype(float)

    results_directory  = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'ct-example'])
    results_directory0 = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'ct-example', 'nonlinear'])
    results_directory1 = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'ct-example', 'linear'])
    results_directory2 = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'ct-example', 'concat'])
    results_directory3 = FileSystem.mkdir(['.', 'results-nonlinear-diffusion', 'ct-example', 'video'])

    """
    if not os.path.isdir(results_directory0):
        os.mkdir(results_directory0, 0o0755)
    if not os.path.isdir(results_directory1):
        os.mkdir(results_directory1, 0o0755)
    if not os.path.isdir(results_directory2):
        os.mkdir(results_directory2, 0o0755)
    if not os.path.isdir(results_directory3):
        os.mkdir(results_directory3, 0o0755)
    """

    if make_images:
        iterations = 119
        image_progress_nonlin = []
        flt = nonlinearDiffusionFilter(org,
                                       iterations=iterations,
                                       lamb=2.0,
                                       image_seq=image_progress_nonlin)
        image_progress_linear = []
        flt = linearDiffusionFilter(org,
                                    iterations=iterations,
                                    g=0.5,
                                    image_seq=image_progress_linear)

        maxvalue = np.amax(org)
        minvalue = np.amin(org)

        for i, img in enumerate(image_progress_nonlin):
            img = makeCtColorTable(img, ct, mask)
            filepath = os.path.join(results_directory0, f'improg-nonlin-{i:03d}.png')
            skimage.io.imsave(filepath, img)

        for i, img in enumerate(image_progress_linear):
            img = makeCtColorTable(img, ct, mask)
            filepath = os.path.join(results_directory1, f'improg-linear-{i:03d}.png')
            skimage.io.imsave(filepath, img)


        for index in range(0, len(image_progress_nonlin)):
            img00 = org
            img01 = image_progress_nonlin[index]
            img02 = image_progress_linear[index]

            img00 = ((255.0 / (maxvalue-minvalue)) * (img00-minvalue)).astype(np.uint8)
            img01 = ((255.0 / (maxvalue-minvalue)) * (img01-minvalue)).astype(np.uint8)
            img02 = ((255.0 / (maxvalue-minvalue)) * (img02-minvalue)).astype(np.uint8)
            img00[mask==0] = 0
            img01[mask==0] = 0
            img02[mask==0] = 0

            img10 = makeCtColorTable(org, ct, mask)
            img11 = makeCtColorTable(image_progress_nonlin[index], ct, mask)
            img12 = makeCtColorTable(image_progress_linear[index], ct, mask)

            img = concatImages(img00, img01, img02, img10, img11, img12)
            img = addTextToCtExample(img, index)
            filepath = os.path.join(results_directory2, f'concat-{index+1:03d}.png')
            skimage.io.imsave(filepath, img)

    make_example = True
    if make_example:
        files0 = FileSystem.getFilesInDirectory(results_directory0) # nonlinear
        files1 = FileSystem.getFilesInDirectory(results_directory1) # linear

        if len(files0) > 0 and len(files1) > 0 :
            img0 = makeCtColorTable(org, ct, mask)
            img1 = skimage.io.imread(files0[-1])
            img2 = skimage.io.imread(files1[-1])
            print(files0[-1])
            print(files1[-1])
            img = concatImages3x(img0, img1, img2)
            img = addTextToCtExample3x(img)
            skimage.io.imsave(os.path.join(results_directory, 'ct-example.png'), img)
        else:
            print("error: Could not load nonlinear or linear CT image example.")

    if make_video:
        sourcedir = os.path.join(results_directory2, '*.png')
        targetdir = results_directory3
        filename = 'ct-diffusion.mp4'
        filepath = os.path.join(targetdir, filename)
        command = f'ffmpeg -framerate 6 -pattern_type glob -i "{sourcedir}" -pix_fmt yuv420p -r 60 -y {filepath}'
        print(command)
        os.system(command)

    make_grad_magnitude = True
    if make_grad_magnitude:
        gm, df = computeGradMagnAndDiffusivity(org, lamb=50.0)

        minv = np.amin(org)
        maxv = np.amax(org)
        dv = maxv - minv
        img0 = 255 * (org - minv) / dv

        minv = np.amin(gm)
        maxv = np.amax(gm)
        dv = maxv - minv
        img1 = 255 * (gm - minv) / dv

        minv = np.amin(df)
        maxv = np.amax(df)
        dv = maxv - minv
        img2 = 255 * (df - minv) / dv

        img0 = img0.astype(np.uint8)
        img1 = img1.astype(np.uint8)
        img2 = img2.astype(np.uint8)
        img0[mask==0] = 0
        img1[mask==0] = 0
        img2[mask==0] = 0

        #img0 = gray2gray3ch(img0)
        #img1 = gray2gray3ch(img1)
        #img2 = gray2gray3ch(img2)

        img = concatImages3x(img0, img1, img2)
        img = img[50:,:,:]
        #img = img[:-62,:,:]
        img[-62:,:,:] = 0

        targetdir = results_directory
        filepath = os.path.join(targetdir, 'ct-gradient-magnitude-diffusivity.png')
        """
        print(img.shape)
        img = addTextToCtExample3x(img, color=(255,255,255), text=['CT', 'Gradmagn', 'Diffusivity $\sigma$'])

        skimage.io.imsave(filepath, img)
        """

        LatexPreamble.latexMatplotlibPreample()
        fig = plt.figure(figsize=(6,2), dpi=300)
        ax: plt.Axes = fig.gca()
        ax.imshow(img, cmap='gray')

        stride = 512

        t0 = 'Image' + '\n' + '$u$'
        t1 = 'Gradient magnitude' + '\n' + r'$|\nabla u|^{2}$'
        t2 = 'Diffusivity' + '\n' + r'$g(|\nabla u|^{2}) ~, \lambda = 50$'

        ax.text(512//2 + 0*stride, 470, t0, color='white', ha='center')
        ax.text(512//2 + 1*stride, 470, t1, color='white', ha='center')
        ax.text(512//2 + 2*stride, 470, t2, color='white', ha='center')

        ax.set_axis_off()
        plt.savefig(filepath, bbox_inches='tight', pad_inches=0)
        plt.close()

    return

# ==============================================================================
# Images with artificial added noise
# ==============================================================================

def saveImage(filepath, image):
    maxv = np.amax(image)
    minv = np.amin(image)
    print(maxv)
    print(minv)
    image = 255.0 * (image-minv) / (maxv-minv)
    image = np.round(image)
    image[image>255.0] = 255.0
    image = image.astype(np.uint8)
    skimage.io.imsave(filepath, image)

def makeNoisyImagesExamplesGuassian():
    filepath = os.path.join('.', 'data', 'spider-cropped-2.jpg')
    image = skimage.io.imread(filepath)
    image = skimage.color.rgb2gray(image)
    print(np.amax(image))

    image = skimage.transform.resize(image, (275, 275), anti_aliasing=True)

    filepath = os.path.join('.', 'results', 'spider-cropped-2-bw.jpg')
    saveImage(filepath, image)

    noisy_image = Image2DNoise.noisy('gauss', image)
    filepath = os.path.join('.', 'results', 'spider-cropped-2-noisy.jpg')
    saveImage(filepath, noisy_image)

    #flt_image = nonlinearDiffusionFilter(noisy_image, iterations=20, lamb=100.0)
    #filepath = os.path.join('.', 'results', 'spider-cropped-2-nonlinear.jpg')
    #saveImage(filepath, flt_image)

    flt_image = linearDiffusionFilter(noisy_image, iterations=20, g=1.0)
    filepath = os.path.join('.', 'results', 'spider-cropped-2-linear.jpg')
    saveImage(filepath, flt_image)

def makeNoisyImagesExamplesSpeckle():

    lamb = 0.0004
    iterations = 1000

    filepath = os.path.join('.', 'data', 'spider-cropped-2.jpg')
    image = skimage.io.imread(filepath)
    image = skimage.color.rgb2gray(image)
    print(np.amax(image))

    image = skimage.transform.resize(image, (275, 275), anti_aliasing=True)

    filepath = os.path.join('.', 'results', 'bw-test', 'spider-cropped-2-bw.png')
    saveImage(filepath, image)

    noisy_image = Image2DNoise.noisy('gauss', image)
    filepath = os.path.join('.', 'results', 'bw-test', 'spider-cropped-2-noisy.png')
    saveImage(filepath, noisy_image)

    flt_image = nonlinearDiffusionFilter(noisy_image, iterations=iterations, lamb=lamb)
    filepath = os.path.join('.', 'results', 'bw-test', 'spider-cropped-2-noisy-nonlinear.png')
    saveImage(filepath, flt_image)

    flt_image = linearDiffusionFilter(noisy_image, iterations=10, g=1.0)
    filepath = os.path.join('.', 'results', 'bw-test', 'spider-cropped-2-noisy-linear.png')
    saveImage(filepath, flt_image)

    flt_image = nonlinearDiffusionFilter(image, iterations=iterations, lamb=lamb)
    filepath = os.path.join('.', 'results', 'bw-test', 'spider-cropped-2-org-nonlinear.png')
    saveImage(filepath, flt_image)

    flt_image = linearDiffusionFilter(image, iterations=10, g=1.0)
    filepath = os.path.join('.', 'results', 'bw-test', 'spider-cropped-2-org-linear.png')
    saveImage(filepath, flt_image)

# ==============================================================================
# Other test images
# ==============================================================================

def fingerprintTest():
    filepath = os.path.join('.', 'data', 'weickert-test-1.png')
    image = skimage.io.imread(filepath)
    image = skimage.color.rgb2gray(image)

    image = image.astype(float)
    flt0= nonlinearDiffusionFilter(image, iterations=30, lamb=0.5)
    flt1= linearDiffusionFilter(image, iterations=30, g=1.0)

    maxvalue0 = np.amax(flt0)
    maxvalue1 = np.amax(flt1)

    flt0 = ((255.0 / maxvalue0) * flt0).astype(np.uint8)
    flt1 = ((255.0 / maxvalue1) * flt1).astype(np.uint8)

    skimage.io.imsave("fingerprints-nonlin.png", flt0)
    skimage.io.imsave("fingerprints-linear.png", flt1)

# ==============================================================================
#
# Main
#
# ==============================================================================
if __name__=='__main__':
    #makeNoisyImagesExamplesGauss()
    #makeNoisyImagesExamplesSpeckle()

    #makeCtExampleImages(make_images=True, make_video=True)
    #makeCtExampleImages(make_images=False, make_video=False)

    copyResultsToDirectory()
