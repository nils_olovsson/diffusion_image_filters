
import matplotlib
import matplotlib.pyplot as plt

def latexMatplotlibPreample():
    matplotlib.use('pgf')
    #plt.rcParams.update({
    #    'font.family': "serif",  # use serif/main font for text elements
    #    'text.usetex': True,     # use inline math for ticks
    #    'pgf.rcfonts': False,    # don't setup fonts from rc parameters
    #    'pgf.preamble': '\n'.join([
    #        r'\usepackage{url}',            # load additional packages
    #        r'\usepackage[no-math]{fontspec}',
    #        r'\usepackage{mathspec}',
    #        r'\setmainfont{Charis SIL}',
    #        r'\setsansfont{Charis SIL}',
    #        r'\setmonofont{Charis SIL}',
    #        r'\setallmainfonts{Charis SIL}'
    #    ])
    #})
    plt.rcParams.update({
        'font.family': "STIX",  # use serif/main font for text elements
        'text.usetex': True,     # use inline math for ticks
        'pgf.rcfonts': False,    # don't setup fonts from rc parameters
        'pgf.preamble': '\n'.join([
            r'\usepackage{url}',            # load additional packages
            r'\usepackage[no-math]{fontspec}',
            r'\usepackage{mathspec}',
            r'\setmainfont{STIX}',
            r'\setsansfont{STIX}',
            r'\setmonofont{STIX}',
            r'\setallmainfonts{STIX}'
        ])
    })
    return
