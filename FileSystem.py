#!/usr/bin/env python3

"""
    FileSystem.py
"""

__author__    = 'Nils Olofsson'
__email__     = 'me@nilsolovsson.se'
__copyright__ = 'Copyright 2022'
__license__   = 'MIT'

import sys
import os

# ------------------------------------------------------------------------------
# Common utilities
# ------------------------------------------------------------------------------

def mkdir(dirs):
    """
        If directories and subdirectories do not exist, create them.
        dirs is a list of subdirectory names,
        mkdir(['foo', 'bar']) -> /foo/bar/
    """
    directory = ''
    for d in dirs:
        directory = os.path.join(directory, d)
        if not os.path.isdir(directory):
            os.mkdir(directory, 0o0755)
    return directory

def getDirectoriesInDirectory(directory, as_full_path=True):
    """
        Get all directories inside directory.
    """
    dirs = []
    try:
        for entry in os.listdir(directory):
            fullpath = os.path.join(directory, entry)
            if entry!='.' and entry!='..' and os.path.isdir(fullpath):
                if as_full_path:
                    dirs.append(fullpath)
                else:
                    dirs.append(entry)
    except:
        print(f"Failed to get directories in {directory}")
        pass
    return dirs

def getFilesInDirectory(directory, ftype=''):
    """
        Get all files of certain extension inside directory.
    """
    files = []
    try:
        for entry in os.listdir(directory):
            filepath = os.path.join(directory, entry)
            _,  ext = os.path.splitext(filepath)
            if os.path.isfile(filepath) and (len(ftype)==0 or ftype==ext):
                files.append(filepath)
    except:
        print(f"Failed to get files in {directory}")
        pass
    return files

def getDicomsInDirectory(directory):
    """
        Get all dicom file inside directory by test reading them.
    """
    dicoms = []
    try:
        for entry in os.listdir(directory):
            file_path = os.path.join(directory, entry)
            if os.path.isfile(file_path):
                try:
                    ds = dcm.dcmread(file_path, stop_before_pixels=True)
                    dicoms.append(file_path)
                except:
                    pass
    except:
        print(f"Failed to get dicoms in {directory}")
        pass
    return dicoms
