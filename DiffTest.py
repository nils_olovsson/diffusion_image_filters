
# ==============================================================================
# Ordinary finite differences
# ==============================================================================

class FD:
    """
        Static class for finite differences
    """

    @staticmethod
    def dx(u: np.ndarray, is_padded: bool = False) -> np.ndarray:
        if not is_padded:
            u = np.pad(u, pad_width=1, mode='constant')
        k = 0.5 * np.array([[ 0.0, 0.0, 0.0],
                            [-1.0, 0.0, 1.0],
                            [ 0.0, 0.0, 0.0]])
        ux = scipy.signal.convolve2d(usig, k, boundary='symm')
        ux = ux[1:-1, 1:-1]
        return ux

    @staticmethod
    def dy(u: np.ndarray, is_padded: bool = False) -> np.ndarray:
        if not is_padded:
            u = np.pad(u, pad_width=1, mode='constant')
        k = 0.5 * np.array([[ 0.0,-1.0, 0.0],
                            [ 0.0, 0.0, 0.0],
                            [ 0.0, 1.0, 0.0]])
        ux = scipy.signal.convolve2d(usig, k, boundary='symm')
        ux = ux[1:-1, 1:-1]
        return ux

# ==============================================================================
# Scharr differences
# ==============================================================================

# ==============================================================================
# Sobel differences
# ==============================================================================

# ==============================================================================
# K optimized differences
# ==============================================================================

# ==============================================================================
# Main
# ==============================================================================

if __name__=='__main__':
    print("Differential tests.")

    filepath = os.path.join('.', 'data', 'weickert-test-2.png')
    image = skimage.io.imread(filepath)
    image = skimage.color.rgb2gray(image)
    image = image.astype(float)

    print(f"Loaded image:\n    {filepath}")
