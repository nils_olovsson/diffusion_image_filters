from setuptools import setup
from Cython.Build import cythonize

import numpy

# Build: python setup.py build_ext --inplace
setup (
    name='DiffusionImageFilterUpdate',
    ext_modules=cythonize("DiffusionImageFilterUpdate.pyx"),
    include_dirs=[numpy.get_include()],
    zip_safe=False
)
