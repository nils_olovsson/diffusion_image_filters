cimport cython

from cython.parallel import prange

import math

import numpy as np

cimport numpy as np

from libcpp cimport bool

from libc.math cimport sqrt

DTYPE = np.uint8
ctypedef np.uint8_t DTYPE_t

DTYPE_INT = np.int32
ctypedef np.int32_t DTYPE_INT_t

DTYPE_FLOAT = np.float32
ctypedef np.float32_t DTYPE_FLOAT_t

DTYPE_DOUBLE = np.float64
ctypedef np.float64_t DTYPE_DOUBLE_t

def sayHello():
    print('Inside cython I am')
    return

# ==============================================================================
# FD derivatives
# ==============================================================================

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def finiteDifferenceSingleChannelX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_x
    cdef double f32, f34

    f32 = f[row+0, col-1]
    f34 = f[row+0, col+1]

    f_x = -f32 + f34
    return (1.0/2.0) * f_x

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def finiteDifferenceSingleChannelY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_y
    cdef double f23, f43

    f23 = f[row-1, col+0]
    f43 = f[row+1, col+0]

    f_y = -f23 + f43
    return (1.0/2.0) * f_y

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def finiteDifferenceMultiChannelX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] f,
    int channel,
    int row,
    int col,
):
    """
    """
    cdef double f_x
    cdef double f32, f34

    f32 = f[channel, row+0, col-1]
    f34 = f[channel, row+0, col+1]

    f_x = -f32 + f34
    return (1.0/2.0) * f_x

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def finiteDifferenceMultiChannelY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] f,
    int channel,
    int row,
    int col,
):
    """
    """
    cdef double f_y
    cdef double f23, f43

    f23 = f[channel, row-1, col+0]
    f43 = f[channel, row+1, col+0]

    f_y = -f23 + f43
    return (1.0/2.0) * f_y

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def finiteDifferenceSingleChannelXX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_xx
    cdef double f32, f33, f34

    f32 = f[row+0, col-1]
    f33 = f[row+0, col+0]
    f34 = f[row+0, col+1]

    f_xx =  f32 - 2*f33 + f34
    return (1.0/4.0) * f_xx

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def finiteDifferenceSingleChannelYY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_xx
    cdef double f32, f33, f34

    f23 = f[row-1, col+0]
    f33 = f[row+0, col+0]
    f43 = f[row+1, col+0]

    f_xx =  f23 - 2*f33 + f43
    return (1.0/4.0) * f_xx

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def finiteDifferenceSingleChannelXY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_xy = 0.0
    cdef double  f22, f42, f24, f44

    f22 = f[row-1, col-1]
    f42 = f[row+1, col-1]
    f24 = f[row-1, col+1]
    f44 = f[row+1, col+1]

    f_xy = -f22 + f42 + f24 - f44

    return (1.0/4.0) * f_xy

# ==============================================================================
# Sobel derivatives
# ==============================================================================

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def sobelSingleChannelX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_x
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    f22 = f[row-1, col-1]
    f32 = f[row+0, col-1]
    f42 = f[row+1, col-1]

    f24 = f[row-1, col+1]
    f34 = f[row+0, col+1]
    f44 = f[row+1, col+1]

    f_x = -f22 - 2*f32 - f42 + f24 + 2*f34 + f44
    return (1.0/8.0) * f_x

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def sobelSingleChannelY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_y
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    f22 = f[row-1, col-1]
    f23 = f[row-1, col+0]
    f24 = f[row-1, col+1]

    f42 = f[row+1, col-1]
    f43 = f[row+1, col+0]
    f44 = f[row+1, col+1]

    f_y = -f22 - 2*f23 - f24 + f42 + 2*f43 + f44
    return (1.0/8.0) * f_y

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def sobelMultiChannelX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] f,
    int channel,
    int row,
    int col,
):
    """
    """
    cdef double f_x
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    f22 = f[channel, row-1, col-1]
    f32 = f[channel, row+0, col-1]
    f42 = f[channel, row+1, col-1]

    f24 = f[channel, row-1, col+1]
    f34 = f[channel, row+0, col+1]
    f44 = f[channel, row+1, col+1]

    f_x = -f22 - 2*f32 - f42 + f24 + 2*f34 + f44
    return (1.0/8.0) * f_x

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def sobelMultiChannelY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] f,
    int channel,
    int row,
    int col,
):
    """
    """
    cdef double f_y
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    f22 = f[channel, row-1, col-1]
    f23 = f[channel, row-1, col+0]
    f24 = f[channel, row-1, col+1]

    f42 = f[channel, row+1, col-1]
    f43 = f[channel, row+1, col+0]
    f44 = f[channel, row+1, col+1]

    f_y = -f22 - 2*f23 - f24 + f42 + 2*f43 + f44
    return (1.0/8.0) * f_y

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def sobelSingleChannelXX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_xx
    cdef double f11, f13, f15
    cdef double f21, f23, f25
    cdef double f31, f33, f35
    cdef double f41, f43, f45
    cdef double f51, f53, f55

    f11 = f[row-2, col-2]
    f21 = f[row-1, col-2]
    f31 = f[row+0, col-2]
    f41 = f[row+1, col-2]
    f51 = f[row+2, col-2]

    f13 = f[row-2, col+0]
    f23 = f[row-1, col+0]
    f33 = f[row+0, col+0]
    f43 = f[row+1, col+0]
    f53 = f[row+2, col+0]

    f15 = f[row-2, col+2]
    f25 = f[row-1, col+2]
    f35 = f[row+0, col+2]
    f45 = f[row+1, col+2]
    f55 = f[row+2, col+2]

    f_xx = 0.0
    f_xx +=  1*f11 + 4*f21 +  6*f31 + 4*f41 + 1*f51
    f_xx += -2*f13 - 8*f23 - 12*f33 - 8*f43 - 2*f53
    f_xx +=  1*f15 + 4*f25 +  6*f35 + 4*f45 + 1*f55
    return (1.0/64.0) * f_xx

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def sobelSingleChannelYY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_yy = 0.0
    cdef double f11, f12, f13, f14, f15
    #cdef double f21, f22, f23, f24, f25
    cdef double f31, f32, f33, f34, f35
    #cdef double f41, f42, f43, f44, f45
    cdef double f51, f52, f53, f54, f55

    f11 = f[row-2, col-2]
    f12 = f[row-2, col-1]
    f13 = f[row-2, col+0]
    f14 = f[row-2, col+1]
    f15 = f[row-2, col+2]

    f31 = f[row+0, col-2]
    f32 = f[row+0, col-1]
    f33 = f[row+0, col+0]
    f34 = f[row+0, col+1]
    f35 = f[row+0, col+2]

    f51 = f[row+2, col-2]
    f52 = f[row+2, col-1]
    f53 = f[row+2, col+0]
    f54 = f[row+2, col+1]
    f55 = f[row+2, col+2]

    f_yy +=  1*f11 + 4*f12 +  6*f13 + 4*f14 + 1*f15
    f_yy += -2*f31 - 8*f32 - 12*f33 - 8*f34 - 2*f35
    f_yy +=  1*f51 + 4*f52 +  6*f53 + 4*f54 + 1*f55
    return (1.0/64.0) * f_yy

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def sobelSingleChannelXY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_xy = 0.0
    cdef double  f11, f12,      f14, f15
    cdef double  f21, f22,      f24, f25
    #cdef double f31, f32, f33, f34, f35
    cdef double  f41, f42,      f44, f45
    cdef double  f51, f52,      f54, f55

    f11 = f[row-2, col-2]
    f12 = f[row-2, col-1]
    #f13 = f[row-2, col+0]
    f14 = f[row-2, col+1]
    f15 = f[row-2, col+2]

    f21 = f[row-1, col-2]
    f22 = f[row-1, col-1]
    #f23 = f[row-1, col+0]
    f24 = f[row-1, col+1]
    f25 = f[row-1, col+2]

    #f31 = f[row+0, col-2]
    #f32 = f[row+0, col-1]
    #f33 = f[row+0, col+0]
    #f34 = f[row+0, col+1]
    #f35 = f[row+0, col+2]

    f41 = f[row+1, col-2]
    f42 = f[row+1, col-1]
    #f43 = f[row+1, col+0]
    f44 = f[row+1, col+1]
    f45 = f[row+1, col+2]

    f51 = f[row+2, col-2]
    f52 = f[row+2, col-1]
    #f53 = f[row+2, col+0]
    f54 = f[row+2, col+1]
    f55 = f[row+2, col+2]

    f_xy +=   1*f11 + 2*f12 - 2*f14 - 1*f15
    f_xy +=   2*f21 + 4*f22 - 4*f24 - 2*f25
    f_xy += - 2*f41 - 4*f42 + 4*f44 + 2*f45
    f_xy += - 1*f51 - 2*f52 + 2*f54 + 1*f55

    return (1.0/64.0) * f_xy

# ==============================================================================
# Scharr derivatives
# ==============================================================================

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def scharrSingleChannelX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_x = 0.0
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    #cdef double v0 = 47
    #cdef double v1 = 162

    cdef double v0 = 3
    cdef double v1 = 10

    f22 = f[row-1, col-1]
    f32 = f[row+0, col-1]
    f42 = f[row+1, col-1]

    f24 = f[row-1, col+1]
    f34 = f[row+0, col+1]
    f44 = f[row+1, col+1]

    f_x += -v0 * f22 - v1 * f32 - v0 * f42
    f_x +=  v0 * f24 + v1 * f34 + v0 * f44
    return (1.0/32.0) * f_x

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def scharrSingleChannelY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_y = 0.0
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    #cdef double v0 = 47
    #cdef double v1 = 162

    cdef double v0 = 3
    cdef double v1 = 10

    f22 = f[row-1, col-1]
    f23 = f[row-1, col+0]
    f24 = f[row-1, col+1]

    f42 = f[row+1, col-1]
    f43 = f[row+1, col+0]
    f44 = f[row+1, col+1]

    f_y += -v0 * f22 - v1 * f23 - v0 * f24
    f_y +=  v0 * f42 + v1 * f43 + v0 * f44
    return (1.0/32.0) * f_y

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def scharrMultiChannelX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] f,
    int channel,
    int row,
    int col,
):
    """
    """
    cdef double f_x = 0.0
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    #cdef double v0 = 47
    #cdef double v1 = 162

    cdef double v0 = 3
    cdef double v1 = 10

    f22 = f[channel, row-1, col-1]
    f32 = f[channel, row+0, col-1]
    f42 = f[channel, row+1, col-1]

    f24 = f[channel, row-1, col+1]
    f34 = f[channel, row+0, col+1]
    f44 = f[channel, row+1, col+1]

    f_x += -v0 * f22 - v1 * f32 - v0 * f42
    f_x +=  v0 * f24 + v1 * f34 + v0 * f44
    return (1.0/32.0) * f_x

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def scharrMultiChannelY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] f,
    int channel,
    int row,
    int col,
):
    """
    """
    cdef double f_y = 0.0
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    #cdef double v0 = 47
    #cdef double v1 = 162

    cdef double v0 = 3
    cdef double v1 = 10

    f22 = f[channel, row-1, col-1]
    f23 = f[channel, row-1, col+0]
    f24 = f[channel, row-1, col+1]

    f42 = f[channel, row+1, col-1]
    f43 = f[channel, row+1, col+0]
    f44 = f[channel, row+1, col+1]

    f_y += -v0 * f22 - v1 * f23 - v0 * f24
    f_y +=  v0 * f42 + v1 * f43 + v0 * f44
    return (1.0/32.0) * f_y

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def scharrSingleChannelXX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_xx
    cdef double f11, f13, f15
    cdef double f21, f23, f25
    cdef double f31, f33, f35
    cdef double f41, f43, f45
    cdef double f51, f53, f55

    #cdef double v0 = 2209
    #cdef double v1 = 4418
    #cdef double v2 = 15228
    #cdef double v3 = 30456
    #cdef double v4 = 30662
    #cdef double v5 = 61324

    cdef double v0 = 9
    cdef double v1 = 18
    cdef double v2 = 60
    cdef double v3 = 120
    cdef double v4 = 118
    cdef double v5 = 236

    f11 = f[row-2, col-2]
    f21 = f[row-1, col-2]
    f31 = f[row+0, col-2]
    f41 = f[row+1, col-2]
    f51 = f[row+2, col-2]

    f13 = f[row-2, col+0]
    f23 = f[row-1, col+0]
    f33 = f[row+0, col+0]
    f43 = f[row+1, col+0]
    f53 = f[row+2, col+0]

    f15 = f[row-2, col+2]
    f25 = f[row-1, col+2]
    f35 = f[row+0, col+2]
    f45 = f[row+1, col+2]
    f55 = f[row+2, col+2]

    f_xx = 0.0
    f_xx +=  v0*f11 + v2*f21 + v4*f31 + v2*f41 + v0*f51
    f_xx += -v1*f13 - v3*f23 - v5*f33 - v3*f43 - v1*f53
    f_xx +=  v0*f15 + v2*f25 + v4*f35 + v2*f45 + v0*f55
    return (1.0/1024.0) * f_xx

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def scharrSingleChannelYY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_yy = 0.0
    cdef double f11, f12, f13, f14, f15
    #cdef double f21, f22, f23, f24, f25
    cdef double f31, f32, f33, f34, f35
    #cdef double f41, f42, f43, f44, f45
    cdef double f51, f52, f53, f54, f55

    #cdef double v0 = 2209
    #cdef double v1 = 4418
    #cdef double v2 = 15228
    #cdef double v3 = 30456
    #cdef double v4 = 30662
    #cdef double v5 = 61324

    cdef double v0 = 9
    cdef double v1 = 18
    cdef double v2 = 60
    cdef double v3 = 120
    cdef double v4 = 118
    cdef double v5 = 236

    f11 = f[row-2, col-2]
    f12 = f[row-2, col-1]
    f13 = f[row-2, col+0]
    f14 = f[row-2, col+1]
    f15 = f[row-2, col+2]

    f31 = f[row+0, col-2]
    f32 = f[row+0, col-1]
    f33 = f[row+0, col+0]
    f34 = f[row+0, col+1]
    f35 = f[row+0, col+2]

    f51 = f[row+2, col-2]
    f52 = f[row+2, col-1]
    f53 = f[row+2, col+0]
    f54 = f[row+2, col+1]
    f55 = f[row+2, col+2]

    f_yy +=  v0*f11 + v2*f12 + v4*f13 + v2*f14 + v0*f15
    f_yy += -v1*f31 - v3*f32 - v5*f33 - v3*f34 - v1*f35
    f_yy +=  v0*f51 + v2*f52 + v4*f53 + v2*f54 + v0*f55
    return (1.0/1024.0) * f_yy

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def scharrSingleChannelXY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    int row,
    int col,
):
    """
    """
    cdef double f_xy = 0.0
    cdef double  f11, f12,      f14, f15
    cdef double  f21, f22,      f24, f25
    #cdef double f31, f32, f33, f34, f35
    cdef double  f41, f42,      f44, f45
    cdef double  f51, f52,      f54, f55

    #cdef double v0 = 2209
    #cdef double v1 = 7614
    #cdef double v2 = 26244

    cdef double v0 = 9
    cdef double v1 = 30
    cdef double v2 = 100

    f11 = f[row-2, col-2]
    f12 = f[row-2, col-1]
    #f13 = f[row-2, col+0]
    f14 = f[row-2, col+1]
    f15 = f[row-2, col+2]

    f21 = f[row-1, col-2]
    f22 = f[row-1, col-1]
    #f23 = f[row-1, col+0]
    f24 = f[row-1, col+1]
    f25 = f[row-1, col+2]

    #f31 = f[row+0, col-2]
    #f32 = f[row+0, col-1]
    #f33 = f[row+0, col+0]
    #f34 = f[row+0, col+1]
    #f35 = f[row+0, col+2]

    f41 = f[row+1, col-2]
    f42 = f[row+1, col-1]
    #f43 = f[row+1, col+0]
    f44 = f[row+1, col+1]
    f45 = f[row+1, col+2]

    f51 = f[row+2, col-2]
    f52 = f[row+2, col-1]
    #f53 = f[row+2, col+0]
    f54 = f[row+2, col+1]
    f55 = f[row+2, col+2]

    f_xy +=   v0*f11 + v1*f12 - v1*f14 - v0*f15
    f_xy +=   v1*f21 + v2*f22 - v2*f24 - v1*f25
    f_xy += - v1*f41 - v2*f42 + v2*f44 + v1*f45
    f_xy += - v0*f51 - v1*f52 + v1*f54 + v0*f55

    return (1.0/1024.0) * f_xy

# ==============================================================================
# Kroon derivatives
# ==============================================================================

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def kroonSingleChannelX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    np.ndarray[DTYPE_DOUBLE_t, ndim=1] p,
    int row,
    int col,
):
    """
    """
    cdef double f_x = 0.0
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    cdef double p13 = p[12]
    cdef double p14 = p[13]

    f22 = f[row-1, col-1]
    f32 = f[row+0, col-1]
    f42 = f[row+1, col-1]

    f24 = f[row-1, col+1]
    f34 = f[row+0, col+1]
    f44 = f[row+1, col+1]

    f_x += -p13 * f22 - p14 * f32 - p13 * f42
    f_x +=  p13 * f24 + p14 * f34 + p13 * f44
    return f_x

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def kroonSingleChannelY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    np.ndarray[DTYPE_DOUBLE_t, ndim=1] p,
    int row,
    int col,
):
    """
    """
    cdef double f_y = 0.0
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    cdef double p13 = p[12]
    cdef double p14 = p[13]

    f22 = f[row-1, col-1]
    f23 = f[row-1, col+0]
    f24 = f[row-1, col+1]

    f42 = f[row+1, col-1]
    f43 = f[row+1, col+0]
    f44 = f[row+1, col+1]

    f_y += -p13 * f22 - p14 * f23 - p13 * f24
    f_y +=  p13 * f42 + p14 * f43 + p13 * f44
    return f_y

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def kroonMultiChannelX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] f,
    np.ndarray[DTYPE_DOUBLE_t, ndim=1] p,
    int channel,
    int row,
    int col,
):
    """
    """
    cdef double f_x = 0.0
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    cdef double p13 = p[12]
    cdef double p14 = p[13]

    f22 = f[channel, row-1, col-1]
    f32 = f[channel, row+0, col-1]
    f42 = f[channel, row+1, col-1]

    f24 = f[channel, row-1, col+1]
    f34 = f[channel, row+0, col+1]
    f44 = f[channel, row+1, col+1]

    f_x += -p13 * f22 - p14 * f32 - p13 * f42
    f_x +=  p13 * f24 + p14 * f34 + p13 * f44
    return f_x

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def kroonMultiChannelY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] f,
    np.ndarray[DTYPE_DOUBLE_t, ndim=1] p,
    int channel,
    int row,
    int col,
):
    """
    """
    cdef double f_y = 0.0
    cdef double f22, f23, f24
    cdef double f32, f33, f34
    cdef double f42, f43, f44

    cdef double p13 = p[12]
    cdef double p14 = p[13]

    f22 = f[channel, row-1, col-1]
    f23 = f[channel, row-1, col+0]
    f24 = f[channel, row-1, col+1]

    f42 = f[channel, row+1, col-1]
    f43 = f[channel, row+1, col+0]
    f44 = f[channel, row+1, col+1]

    f_y += -p13 * f22 - p14 * f23 - p13 * f24
    f_y +=  p13 * f42 + p14 * f43 + p13 * f44
    return f_y

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def kroonSingleChannelXX(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    np.ndarray[DTYPE_DOUBLE_t, ndim=1] p,
    int row,
    int col,
):
    """
    """
    cdef double f_xx
    cdef double f11, f12, f13, f14, f15
    cdef double f21, f22, f23, f24, f25
    cdef double f31, f32, f33, f34, f35
    cdef double f41, f42, f43, f44, f45
    cdef double f51, f52, f53, f54, f55

    cdef double p1 = p[0]
    cdef double p2 = p[1]
    cdef double p3 = p[2]
    cdef double p4 = p[3]
    cdef double p5 = p[4]
    cdef double p6 = p[5]
    cdef double p7 = p[6]
    cdef double p8 = p[7]
    cdef double p9 = p[8]

    f11 = f[row-2, col-2]
    f21 = f[row-1, col-2]
    f31 = f[row+0, col-2]
    f41 = f[row+1, col-2]
    f51 = f[row+2, col-2]

    f12 = f[row-2, col-1]
    f22 = f[row-1, col-1]
    f32 = f[row+0, col-1]
    f42 = f[row+1, col-1]
    f52 = f[row+2, col-1]

    f13 = f[row-2, col+0]
    f23 = f[row-1, col+0]
    f33 = f[row+0, col+0]
    f43 = f[row+1, col+0]
    f53 = f[row+2, col+0]

    f14 = f[row-2, col+1]
    f24 = f[row-1, col+1]
    f34 = f[row+0, col+1]
    f44 = f[row+1, col+1]
    f54 = f[row+2, col+1]

    f15 = f[row-2, col+2]
    f25 = f[row-1, col+2]
    f35 = f[row+0, col+2]
    f45 = f[row+1, col+2]
    f55 = f[row+2, col+2]

    f_xx = 0.0
    f_xx +=  p1*f11 + p2*f21 + p3*f31 + p2*f41 + p1*f51
    f_xx +=  p4*f12 + p5*f22 + p6*f32 + p5*f42 + p4*f52
    f_xx += -p7*f13 - p8*f23 - p9*f33 - p8*f43 - p7*f53
    f_xx +=  p4*f14 + p5*f24 + p6*f34 + p5*f44 + p4*f54
    f_xx +=  p1*f15 + p2*f25 + p3*f35 + p2*f45 + p1*f55
    return f_xx

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def kroonSingleChannelYY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    np.ndarray[DTYPE_DOUBLE_t, ndim=1] p,
    int row,
    int col,
):
    """
    """
    cdef double f_yy = 0.0
    cdef double f11, f12, f13, f14, f15
    cdef double f21, f22, f23, f24, f25
    cdef double f31, f32, f33, f34, f35
    cdef double f41, f42, f43, f44, f45
    cdef double f51, f52, f53, f54, f55

    cdef double p1 = p[0]
    cdef double p2 = p[1]
    cdef double p3 = p[2]
    cdef double p4 = p[3]
    cdef double p5 = p[4]
    cdef double p6 = p[5]
    cdef double p7 = p[6]
    cdef double p8 = p[7]
    cdef double p9 = p[8]

    f11 = f[row-2, col-2]
    f21 = f[row-1, col-2]
    f31 = f[row+0, col-2]
    f41 = f[row+1, col-2]
    f51 = f[row+2, col-2]

    f12 = f[row-2, col-1]
    f22 = f[row-1, col-1]
    f32 = f[row+0, col-1]
    f42 = f[row+1, col-1]
    f52 = f[row+2, col-1]

    f13 = f[row-2, col+0]
    f23 = f[row-1, col+0]
    f33 = f[row+0, col+0]
    f43 = f[row+1, col+0]
    f53 = f[row+2, col+0]

    f14 = f[row-2, col+1]
    f24 = f[row-1, col+1]
    f34 = f[row+0, col+1]
    f44 = f[row+1, col+1]
    f54 = f[row+2, col+1]

    f15 = f[row-2, col+2]
    f25 = f[row-1, col+2]
    f35 = f[row+0, col+2]
    f45 = f[row+1, col+2]
    f55 = f[row+2, col+2]

    f_yy +=  p1*f11 + p2*f12 + p3*f13 + p2*f14 + p1*f15
    f_yy +=  p4*f21 + p5*f22 + p6*f23 + p5*f24 + p4*f25
    f_yy += -p7*f31 - p8*f32 - p9*f33 - p8*f34 - p7*f35
    f_yy +=  p4*f31 + p5*f32 + p6*f33 + p5*f34 + p4*f35
    f_yy +=  p1*f51 + p2*f52 + p3*f53 + p2*f54 + p1*f55
    return f_yy

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def kroonSingleChannelXY(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] f,
    np.ndarray[DTYPE_DOUBLE_t, ndim=1] p,
    int row,
    int col,
):
    """
    """
    cdef double f_xy = 0.0
    cdef double  f11, f12,      f14, f15
    cdef double  f21, f22,      f24, f25
    #cdef double f31, f32, f33, f34, f35
    cdef double  f41, f42,      f44, f45
    cdef double  f51, f52,      f54, f55

    cdef double p10 = p[9]
    cdef double p11 = p[10]
    cdef double p12 = p[11]

    f11 = f[row-2, col-2]
    f12 = f[row-2, col-1]
    #f13 = f[row-2, col+0]
    f14 = f[row-2, col+1]
    f15 = f[row-2, col+2]

    f21 = f[row-1, col-2]
    f22 = f[row-1, col-1]
    #f23 = f[row-1, col+0]
    f24 = f[row-1, col+1]
    f25 = f[row-1, col+2]

    #f31 = f[row+0, col-2]
    #f32 = f[row+0, col-1]
    #f33 = f[row+0, col+0]
    #f34 = f[row+0, col+1]
    #f35 = f[row+0, col+2]

    f41 = f[row+1, col-2]
    f42 = f[row+1, col-1]
    #f43 = f[row+1, col+0]
    f44 = f[row+1, col+1]
    f45 = f[row+1, col+2]

    f51 = f[row+2, col-2]
    f52 = f[row+2, col-1]
    #f53 = f[row+2, col+0]
    f54 = f[row+2, col+1]
    f55 = f[row+2, col+2]

    f_xy +=   p10*f11 + p11*f12 - p11*f14 - p10*f15
    f_xy +=   p11*f21 + p12*f22 - p12*f24 - p11*f25
    f_xy += - p11*f41 - p12*f42 + p12*f44 + p11*f45
    f_xy += - p10*f51 - p11*f52 + p11*f54 + p10*f55

    return f_xy

# ==============================================================================
# Anisotropic update
# ==============================================================================

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def gradU(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] u,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] u_dx,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] u_dy,
    int kernal_type,
):
    """
        Compute the gradient of u.
    """

    cdef int i, j
    cdef int N = u.shape[1]
    cdef int M = u.shape[0]
    p = np.array([0.00549691757211334, 4.75686155670860e-10, 3.15405721902292e-11, 0.00731109320628158, 1.40937549145842e-10, 0.0876322157772825, 0.0256808495553998, 5.87110587298283e-11, 0.171008417902939, 3.80805359553021e-12, 9.86953381462523e-12, 0.0231020787600445, 0.00638922328831119, 0.0350184289706385], dtype=np.double)

    for i in range(1, N-1):
        for j in range(1, M-1):
            #u_dx[j-1,i-1] = kroonSingleChannelX(u, p, j, i)
            #u_dy[j-1,i-1] = kroonSingleChannelY(u, p, j, i)
            u_dx[j-1,i-1] = scharrSingleChannelX(u, j, i)
            u_dy[j-1,i-1] = scharrSingleChannelY(u, j, i)
    return None

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def computeUpdateLinear1Ch(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] u,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] update,
    double g
):
    """
        Comput the update for the next iteration using the spatial
        derivatives.
    """
    #update = np.zeros(u.shape, dtype=float)
    #u = np.pad(u, pad_width=1, mode='constant')

    cdef double g_pj;
    cdef double g_nj;
    cdef double g_ip;
    cdef double g_in;
    cdef double ux0;
    cdef double ux1;
    cdef double uy0;
    cdef double uy1;
    cdef int N = u.shape[1]
    cdef int M = u.shape[0]
    cdef int i, j
    for i in range(1, N-1):
        for j in range(1, M-1):

            g_pj = g
            g_nj = g
            g_ip = g
            g_in = g

            if i==u.shape[1]-2:
                g_pj = 0
            if i==1:
                g_nj = 0
            if j==u.shape[0]-2:
                g_ip = 0
            if j==1:
                g_in = 0

            ux0 =   g_pj * (u[j, i+1] - u[j, i])
            ux1 = - g_nj * (u[j, i]   - u[j, i-1])
            uy0 =   g_ip * (u[j+1, i] - u[j, i])
            uy1 = - g_in * (u[j, i]   - u[j-1, i])

            update[j-1,i-1] = ux0 + ux1 + uy0 + uy1
    return update

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def computeUpdateNonLinear1Ch(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] u,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] update,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] g,
):
    """
        Compute the update for the next iteration using the spatial
        derivatives.
    """
    cdef double g_pj;
    cdef double g_nj;
    cdef double g_ip;
    cdef double g_in;
    cdef double ux0;
    cdef double ux1;
    cdef double uy0;
    cdef double uy1;
    cdef int i, j
    cdef int N = u.shape[1]
    cdef int M = u.shape[0]
    for i in range(1, N-1):
        for j in range(1, M-1):

            g_pj = sqrt(g[j, i+1] * g[j, i])
            g_nj = sqrt(g[j, i-1] * g[j, i])
            g_ip = sqrt(g[j+1, i] * g[j, i])
            g_in = sqrt(g[j-1, 1] * g[j, i])

            if i==u.shape[1]-2:
                g_pj = 0
            if i==1:
                g_nj = 0
            if j==u.shape[0]-2:
                g_ip = 0
            if j==1:
                g_in = 0

            ux0 =   g_pj * (u[j, i+1] - u[j, i])
            ux1 = - g_nj * (u[j, i]   - u[j, i-1])
            uy0 =   g_ip * (u[j+1, i] - u[j, i])
            uy1 = - g_in * (u[j, i]   - u[j-1, i])

            update[j-1,i-1] = ux0 + ux1 + uy0 + uy1
    return update

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def computeImageStructureTensors(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] grad_x,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] grad_y,
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] J,
):
    """
        Compute the matrix valued image structure for anisotropic diffusion
        image filter.
    """

    cdef double gx
    cdef double gy

    cdef int i, j
    cdef int N = grad_x.shape[1]
    cdef int M = grad_x.shape[0]

    for i in range(0, N):
        for j in range(0, M):
            gx = grad_x[j, i]
            gy = grad_y[j, i]
            J[0, j, i] = gx * gx
            J[1, j, i] = gx * gy
            J[2, j, i] = gx * gy
            J[3, j, i] = gy * gy
    return J

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def computeAnisotropicDiffusivity(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] J,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] grad_magnitude,
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] D,
    double alpha,
    int    mode,
):
    """
        Compute the matrix valued anisotropic diffusivity.
    """

    #DEF C = 3.315
    DEF C = 1e-3
    DEF lamb = 4

    cdef double j11
    cdef double j12
    cdef double j22

    cdef double j11_j22
    cdef double sqrt_term

    cdef double vnorm

    cdef double v1a
    cdef double v1b

    cdef double v2a
    cdef double v2b

    cdef double l1
    cdef double l2

    cdef double m1
    cdef double m2
    cdef double dm

    cdef double a
    cdef double bc
    cdef double d

    cdef int i, j
    cdef int N = J.shape[2]
    cdef int M = J.shape[1]

    for i in range(0, N):
        for j in range(0, M):
            j11 = J[0,j,i]
            j12 = J[1,j,i]
            j22 = J[3,j,i]

            j11_j22 = j11 - j22
            sqrt_term = math.sqrt(j11_j22 * j11_j22 + 4.0 * j12 * j12)

            v1a = 2.0 * j12
            v1b = j22 - j11 + sqrt_term

            vnorm = math.sqrt(v1a*v1a + v1b*v1b)
            if vnorm < 1.e-8:
                dm = grad_magnitude[j,i]
            else:
                v1a /= vnorm
                v1b /= vnorm

            v2a =  v1b
            v2b = -v1a

            m1 = 0.5 * (j11 + j22 + sqrt_term)
            m2 = 0.5 * (j11 + j22 - sqrt_term)

            # Sort the two eigenvalues
            #if abs(m1) < abs(m2):
            #    m1, m2 = m2, m1
            #    v1a, v2a = v2a, v1a
            #    v1b, v2b = v2b, v1b
            #dm = abs(abs(m2) - abs(m1))

            if mode == 0:
                # Edge enhancing
                l2 = 1.0
                l1 = 1.0
                if abs(m1) > 1.e-10 :
                    l1 = 1.0 - math.exp(-C/ (m1**4.0 + 1.e-10))
            elif mode == 1:
                # Coherence enhancing
                l1 = alpha
                l2 = alpha
                dm = abs(m1 - m2)
                if dm > 1.e-10 :
                    l2 = alpha + (1.0 - alpha) * math.exp( -1 / (dm*dm) )
            elif mode == 2:
                # Directed diffusion, smear along
                l1 = 0.0
                l2 = 0.9
            elif mode == 3:
                # Directed diffusion, smear across
                l1 = 0.9
                l2 = 0.0
            else:
                raise Exception(f"error: computeAnisotropicDiffusivity()\n    mode not set to valid number (0-2), mode={mode}.")

            # Store the anisotropic diffusivity in array
            a  = l1 * v1a * v1a + l2 * v2a * v2a
            bc = l1 * v1a * v1b + l2 * v2a * v2b
            d  = l1 * v1b * v1b + l2 * v2b * v2b

            D[0,j,i] = a
            D[1,j,i] = bc
            D[2,j,i] = bc
            D[3,j,i] = d
    return D

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def computeUpdateAnisotropic1Ch(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] u,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] update,
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] D,
    int zero_div,
):
    """
        Comput the update for the next iteration using the spatial
        derivatives.
    """

    cdef double u11, u12, u13
    cdef double u21, u22, u23
    cdef double u31, u32, u33

    cdef double a23, a21, a22
    cdef double b32, b12, b22
    cdef double c23, c21, c22
    cdef double d32, d12, d22

    cdef double divDnablau
    cdef double trDnablanablau

    cdef int i, j
    cdef int N = u.shape[1]
    cdef int M = u.shape[0]

    #for i in range(1, N-1):
    for i in range(1, N-1):
        for j in range(1, M-1):

            a21 = D[0, j-1, i+0]
            a22 = D[0, j+0, i+0]
            a23 = D[0, j+1, i+0]

            b12 = D[1, j+0, i-1]
            b22 = D[1, j+0, i+0]
            b32 = D[1, j+0, i+1]

            c21 = D[2, j-1, i+0]
            c22 = D[2, j+0, i+0]
            c23 = D[2, j+1, i+0]

            d12 = D[3, j+0, i-1]
            d22 = D[3, j+0, i+0]
            d32 = D[3, j+0, i+1]

            u11 = u[j-1, i-1]
            u12 = u[j+0, i-1]
            u13 = u[j+1, i-1]

            u21 = u[j-1, i+0]
            u22 = u[j+0, i+0]
            u23 = u[j+1, i+0]

            u31 = u[j-1, i+1]
            u32 = u[j+0, i+1]
            u33 = u[j+1, i+1]

            # Complete differential expressions
            divDnablau     = 0.25 * (a23 - a21 + b32 - b12) * (u23 - u21) + 0.25 * (c23 - c21 + d32 - d12) * (u32 - u12)
            trDnablanablau = a22 * (u23 - 2.0*u22 + u21) + d22 * (u32 - 2.0*u22 + u12) + 0.25 * (b22 + c22) * ((u33 - u31) - (u13 - u11))

            if zero_div == 0:
                update[j-1,i-1] = divDnablau + trDnablanablau
            else:
                update[j-1,i-1] = trDnablanablau
    return update

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def computeUpdateAnisotropicKernel1Ch(
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] u,
    np.ndarray[DTYPE_DOUBLE_t, ndim=2] update,
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] D,
    int kernel_type,
    int zero_div,
):
    """
        Compute the update for the next iteration using the spatial
        derivatives.
    """

    cdef double a, b, c, d

    cdef double a_x, b_y, c_x, d_y
    cdef double u_x, u_y, u_xx, u_yy, u_xy

    cdef double divDnablau
    cdef double trDnablanablau

    cdef int a_chan = 0
    cdef int b_chan = 1
    cdef int c_chan = 2
    cdef int d_chan = 3

    cdef int i, j
    cdef int N = u.shape[1]
    cdef int M = u.shape[0]

    p = np.array([0.00549691757211334, 4.75686155670860e-10, 3.15405721902292e-11, 0.00731109320628158, 1.40937549145842e-10, 0.0876322157772825, 0.0256808495553998, 5.87110587298283e-11, 0.171008417902939, 3.80805359553021e-12, 9.86953381462523e-12, 0.0231020787600445, 0.00638922328831119, 0.0350184289706385], dtype=np.double)

    for i in range(2, N-2):
        for j in range(2, M-2):

            a = D[a_chan, j, i]
            b = D[b_chan, j, i]
            c = D[c_chan, j, i]
            d = D[d_chan, j, i]


            if kernel_type == 0:
                a_x = finiteDifferenceMultiChannelX(D, a_chan, j, i)
                b_y = finiteDifferenceMultiChannelY(D, b_chan, j, i)
                c_x = finiteDifferenceMultiChannelX(D, c_chan, j, i)
                d_y = finiteDifferenceMultiChannelY(D, d_chan, j, i)

                u_x = finiteDifferenceSingleChannelX(u, j, i)
                u_y = finiteDifferenceSingleChannelY(u, j, i)

                u_xx = finiteDifferenceSingleChannelXX(u, j, i)
                u_yy = finiteDifferenceSingleChannelYY(u, j, i)
                u_xy = finiteDifferenceSingleChannelXY(u, j, i)
            elif kernel_type == 1:
                a_x = sobelMultiChannelX(D, a_chan, j, i)
                b_y = sobelMultiChannelY(D, b_chan, j, i)
                c_x = sobelMultiChannelX(D, c_chan, j, i)
                d_y = sobelMultiChannelY(D, d_chan, j, i)

                u_x = sobelSingleChannelX(u, j, i)
                u_y = sobelSingleChannelY(u, j, i)

                u_xx = sobelSingleChannelXX(u, j, i)
                u_yy = sobelSingleChannelYY(u, j, i)
                u_xy = sobelSingleChannelXY(u, j, i)
            elif kernel_type == 2:
                a_x = scharrMultiChannelX(D, a_chan, j, i)
                b_y = scharrMultiChannelY(D, b_chan, j, i)
                c_x = scharrMultiChannelX(D, c_chan, j, i)
                d_y = scharrMultiChannelY(D, d_chan, j, i)

                u_x = scharrSingleChannelX(u, j, i)
                u_y = scharrSingleChannelY(u, j, i)

                u_xx = scharrSingleChannelXX(u, j, i)
                u_yy = scharrSingleChannelYY(u, j, i)
                u_xy = scharrSingleChannelXY(u, j, i)
            elif kernel_type == 3:
                a_x = kroonMultiChannelX(D, p, a_chan, j, i)
                b_y = kroonMultiChannelY(D, p, b_chan, j, i)
                c_x = kroonMultiChannelX(D, p, c_chan, j, i)
                d_y = kroonMultiChannelY(D, p, d_chan, j, i)

                u_x = kroonSingleChannelX(u, p, j, i)
                u_y = kroonSingleChannelY(u, p, j, i)

                u_xx = kroonSingleChannelXX(u, p, j, i)
                u_yy = kroonSingleChannelYY(u, p, j, i)
                u_xy = kroonSingleChannelXY(u, p, j, i)

            # Complete differential expressions
            divDnablau     = (a_x + b_y) * u_x + (c_x + d_y) * u_y
            trDnablanablau = a * u_xx + d * u_yy + (b + d) * u_xy

            if zero_div == 0:
                update[j-2,i-2] = divDnablau + trDnablanablau
            else:
                update[j-2,i-2] = trDnablanablau
    return update

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def computeUpdateAnisotropicKernelNCh(
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] u,
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] update,
    np.ndarray[DTYPE_DOUBLE_t, ndim=3] D,
    int kernal_type,
    int zero_div,
):
    """
        Compute the update for the next iteration using the spatial
        derivatives.
    """

    for channel in range(u.shape[2]):
        update[:,:,channel] = computeUpdateAnisotropicKernel1Ch(u[:,:,channel], update[:,:,channel], D, kernal_type, zero_div)
    return update
