#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

import os
import sys

import curses
import shutil
from pathlib import Path

from typing import Tuple, List

import numpy as np
import scipy.signal
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

import pydicom
import skimage.io
import skimage.exposure
import skimage.transform

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import FileSystem
import LatexPreamble

import Image2DNoise

from AnisotropicDiffusionImageFilter import anisotropicDiffusionFilter
from AnisotropicDiffusionImageFilterNumpy import anisotropicDiffusionFilterNumpy

def rgb2gray(img):
    ndim = len(img.shape)
    gr8 = img
    if ndim == 3:
        if img.shape[2]==3:
            gr8 = skimage.color.rgb2gray(img)
        elif img.shape[2]==4:
            gr8 = skimage.color.rgb2gray(skimage.color.rgba2rgb(img))
    return gr8

def gray2gray3ch(img):
    w, h = img.shape
    trg = np.zeros((w,h,3), dtype=img.dtype)
    trg[:,:,0] = img
    trg[:,:,1] = img
    trg[:,:,2] = img
    return trg

def getPilImageFont(fontsize=12.0):
    fontpath = os.path.join('.', 'fonts', 'ttf', 'STIXGeneral.ttf')
    font = ImageFont.truetype(fontpath, fontsize)
    return font

# ==============================================================================
# Make video
# ==============================================================================

def makeVideo(
    image_filename: str,
    settings_str: str,
    framerate: int = 6,
    id_str: str = '',
) -> None:

    settings_str = '-'.join(settings_str.split('-')[1:-2])

    results_directory = FileSystem.mkdir(['.', 'results-anisotropic-diffusion'])

    sourcedir = os.path.join(results_directory, Path(image_filename).stem, settings_str, '*.png')
    targetdir = FileSystem.mkdir([results_directory, 'video'])
    filename = f'{Path(image_filename).stem}-video.mp4'
    if id_str != '':
        filename = f'{Path(image_filename).stem}-{id_str}-video.mp4'
    filepath = os.path.join(targetdir, filename)
    command = f'ffmpeg -framerate {framerate} -pattern_type glob -i "{sourcedir}" -pix_fmt yuv420p -r 60 -y {filepath}'
    print(command)
    os.system(command)
    return

# ==============================================================================
# Generic functions for running the image tests
# ==============================================================================

def settings2str(
    mode,
    iterations,
    sigma,
    rho,
    alpha,
    tau,
    kernel_type,
    zero_div,
):

    kernel_type_str = {}
    kernel_type_str[0] = 'fd'
    kernel_type_str[1] = 'sobel'
    kernel_type_str[2] = 'scharr'
    kernel_type_str[3] = 'kroon'

    mode_type_str = {}
    mode_type_str[0] = 'wckedge'
    mode_type_str[1] = 'wckcoherence'
    mode_type_str[2] = 'smear-along'
    mode_type_str[3] = 'smear-cross'

    s = f'{sigma:05.2f}'
    r = f'{rho:05.2f}'
    a = f'{alpha:.2E}'.replace('.',':')
    t = f'{tau:.2E}'.replace('.',':')

    zero_div_str = str(bool(zero_div)).lower()

    s = f'-{{{kernel_type_str[kernel_type]}}}-{{{mode_type_str[mode]}}}-s{{{s}}}-r{{{r}}}-a{{{a}}}-zdiv-{{{zero_div_str}}}-it-{iterations:04d}'
    return s

def saveResult(
    image,
    original_filename,
    result_directory,
    mode,
    iterations,
    sigma,
    rho,
    alpha,
    tau,
    kernel_type,
    zero_div,
    curses_win
):

    rescale_intensity = False

    if image.shape[0]%2 == 1:
        image = image[1:,:]
    if image.shape[1]%2 == 1:
        image = image[:,1:]

    if rescale_intensity:
        minvalue0 = np.amin(image)
        image -= minvalue0
        maxvalue0 = np.amax(image)
        print(f'min: {minvalue0}')
        print(f'max: {maxvalue0}')
        image = 1.0/maxvalue0 * image
        #image = skimage.exposure.adjust_sigmoid(image)
        maxvalue0 = np.amax(image)
        image = ((255.0 / maxvalue0) * image).astype(np.uint8)
    else:
        image = np.round(image)
        image[image>255.0] = 255.0
        image[image<  0.0] = 0.0
        image = image.astype(np.uint8)

    settings_str = settings2str(mode, iterations, sigma, rho, alpha, tau, kernel_type, zero_div)
    settings_dcs = '-'.join(settings_str.split('-')[1:-2])

    result_directory = FileSystem.mkdir([result_directory, settings_dcs])

    result_filename = Path(original_filename).stem
    result_filename += settings_str
    result_filename += Path(original_filename).suffix

    if not os.path.isdir(result_directory):
        if curses_win:
            curses_win.addstr(3, 0, "error: Failed to save file!")
            curses_win.addstr(4, 0, result_directory[0:79])
            curses_win.refresh()
        else:
            print("error: Failed to save file!")
            print(result_directory[0:79])
    else:
        if curses_win:
            curses_win.addstr(3, 0, "Saving image!")
            curses_win.addstr(4, 0, result_filename[0:79])
            curses_win.refresh()
        else:
            print("Saving image!")
            print(result_filename[0:79])

    resultpath = os.path.join(result_directory, result_filename)
    skimage.io.imsave(resultpath, image)
    return

def anisotropicDiffusionImageFilterTest(
    filename,
    modes,
    kernel_types,
    iteration_options,
    sigma,
    rho,
    alpha,
    tau,
    zero_div,
    color_diffusion = False,
):
    filepath = os.path.join('.', 'data', filename)
    image = skimage.io.imread(filepath)
    if color_diffusion:
        filename = filename.split('.')[0] + '-color.' + filename.split('.')[1]
        image = image[:,:,0:3] # Remove alpha channel
        #for c in range(image.shape[2]):
        #    image[:,:,c] = scipy.ndimage.gaussian_filter(image[:,:,c], sigma=1.5)
    else:
        image = rgb2gray(image)

    print(image.shape)

    minvalue0 = np.amin(image)
    image -= minvalue0
    maxvalue0 = np.amax(image)
    image = 1.0/maxvalue0 * image

    image = 255 * image.astype(float)

    result_directory = FileSystem.mkdir(['.', 'results-anisotropic-diffusion', Path(filename).stem])

    curses.initscr()
    curses_win = curses.newwin(6, 81, 0, 0)
    curses_win.addstr(0, 0, filename)
    curses_win.refresh()
    #curses_win = None

    if not isinstance(sigma, List):
        sigma = [sigma]

    if not isinstance(rho, List):
        rho = [rho]

    if not isinstance(alpha, List):
        alpha = [alpha]

    try:
        for mode in modes:
            for kernel_type in kernel_types:
                for s in sigma:
                    for r in rho:
                        for a in alpha: 
                            iterations = iteration_options[-1]

                            msg = settings2str(mode, iterations, s, r, a, tau, kernel_type, zero_div)
                            msg = msg[0:79]
                            if curses_win:
                                curses_win.addstr(1, 0, msg)
                                curses_win.refresh()
                            else:
                                print(msg)

                            results_seq = []
                            """
                            flt0 = anisotropicDiffusionFilter(
                                image,
                                iterations = iterations,
                                sigma = s,
                                rho   = r,
                                alpha = a,
                                mode  = mode,
                                tau   = tau,
                                image_seq = results_seq,
                                kernel_type = kernel_type,
                                zero_div = zero_div,
                                curses_win=curses_win,
                            )
                            """
                            flt0 = anisotropicDiffusionFilterNumpy(
                                image,
                                iterations = iterations,
                                sigma = s,
                                rho   = r,
                                alpha = a,
                                mode  = mode,
                                tau   = tau,
                                image_seq = results_seq,
                                kernel_type = kernel_type,
                                zero_div = zero_div,
                                curses_win=curses_win,
                            )

                            if len(results_seq) == (iteration_options[-1]+1):
                                for iterations in iteration_options:
                                    saveResult(
                                        results_seq[iterations],
                                        original_filename = filename,
                                        result_directory = result_directory,
                                        iterations = iterations,
                                        sigma = s,
                                        rho   = r,
                                        alpha = a,
                                        mode  = mode,
                                        tau   = tau,
                                        kernel_type = kernel_type,
                                        zero_div = zero_div,
                                        curses_win=curses_win,
                                    )
                                del results_seq
                            else:
                                print("error: results not saved to disk!")
                                print(len(results_seq))
                                print(iteration_options[-1]+1)
    except Exception as e:
        if curses_win:
            curses.endwin()
            curses_win = None
        print("Exception!!!")
        print(e)
    
    if curses_win:
        curses.endwin()
    return

# ==============================================================================
# Running tests on images
# ==============================================================================

def objectsTest():
    filename = 'weickert-object.png'

    #modes = [0,1,2]
    modes = [0]
    kernel_types = range(0,4)
    iteration_options = [0,10,20,100,200]

    sigma = [1.0]
    rho   = [0.0]
    alpha = 0.001
    tau   = 0.125
    zero_div = 0

    anisotropicDiffusionImageFilterTest(
        filename,
        modes,
        kernel_types,
        iteration_options,
        sigma,
        rho,
        alpha,
        tau,
        zero_div)
    return

def fingerprintTest():
    filename = 'weickert-fingerprint-2.png'

    #modes = [0,1,2]
    modes = [1]
    #kernel_types = range(1,4)
    #kernel_types = [0,3]
    kernel_types = [3]
    iteration_options = [10,20,40,80,120,160,200,300,400,600,1200]
    #
    iteration_options = range(0,161)

    sigma = 0.5
    rho   = 0.5
    alpha = 0.0001
    tau   = 0.125
    zero_div = 0

    anisotropicDiffusionImageFilterTest(
        filename,
        modes,
        kernel_types,
        iteration_options,
        sigma,
        rho,
        alpha,
        tau,
        zero_div)
    settings_str = settings2str(mode=1, iterations=iteration_options[-1], sigma=sigma, rho=rho, alpha=alpha, tau=tau, kernel_type=3, zero_div=zero_div)
    makeVideo(filename, settings_str, framerate=12)

    return

def fingerprintArtifactsTest():
    filename = 'weickert-fingerprint-2.png'

    modes = [1]
    kernel_types = [2]
    iteration_options = [10,20,40,80,120,160,200,300,400,600,1200]
    #
    iteration_options = range(0,401)

    sigma = 0.5
    rho   = 0.5
    alpha = 0.0001
    tau   = 0.125
    zero_div = 0
    
    anisotropicDiffusionImageFilterTest(
        filename,
        modes,
        kernel_types,
        iteration_options,
        sigma,
        rho,
        alpha,
        tau,
        zero_div)
    
    settings_str = settings2str(mode=1, iterations=iteration_options[-1], sigma=sigma, rho=rho, alpha=alpha, tau=tau, kernel_type=2, zero_div=zero_div)
    makeVideo(filename, settings_str, framerate=30, id_str='artifacts')

    return

def fiberTest():
    filename = 'weickert-fiber.png'

    modes = [1]
    kernel_types = [3]
    iteration_options = [10,20,40,80,120,160,200,300,400]
    iteration_options = [0,10,20,40,80,120,160,200,300,400,800,1200,1600,2000]

    sigma = 0.5
    rho   = 0.5
    alpha = 0.0001
    tau   = 0.5*0.125
    zero_div = 0

    anisotropicDiffusionImageFilterTest(
        filename,
        modes,
        kernel_types,
        iteration_options,
        sigma,
        rho,
        alpha,
        tau,
        zero_div)
    return

def vangoghColorTest1():
    filename = 'van-gogh-field-with-ploughman-tiny.png'
    filepath = os.path.join('.', 'data', filename)
    image = skimage.io.imread(filepath)
    image = image.astype(float)

    zero_div = 0
    iteration_options = [0,10,50,100,200,250,300,350,400]
    iteration_options = range(0,601)
    #for i in range(len(iteration_options)):
    #    iteration_options[i] = 2 * iteration_options[i]
    #iteration_options = [10,50,100]

    #result_directory = FileSystem.mkdir(['.', 'results-anisotropic-diffusion', Path(filename).stem])

    sigma = [0.5, 1.0, 2.0, 4.0, 5.0]
    rho   = [0.5, 1.0, 2.0, 4.0, 5.0]

    sigma = 1.0
    rho   = 0.0

    alpha = 0.001
    tau   = 0.125

    #modes = [1,2]
    #kernel_types = [2,3]

    modes = [1]
    kernel_types = [3]

    anisotropicDiffusionImageFilterTest(
        filename,
        modes,
        kernel_types,
        iteration_options,
        sigma,
        rho,
        alpha,
        tau,
        zero_div,
        color_diffusion = True)
    
    filename = filename.split('.')[0] + '-color.' + filename.split('.')[1]
    settings_str = settings2str(mode=1, iterations=iteration_options[-1], sigma=sigma, rho=rho, alpha=alpha, tau=tau, kernel_type=3, zero_div=zero_div)
    makeVideo(filename, settings_str, framerate=20)
    
    return

def vangoghColorTest2():
    filename = 'van-gogh-road-with-cypress-and-star-tiny.png'
    filepath = os.path.join('.', 'data', filename)
    image = skimage.io.imread(filepath)
    image = image.astype(float)

    zero_div = 0
    iteration_options = [0,10,20,30,40,50,100,200,250,300,350,400,500]
    #iteration_options = [10,20]
    #for i in range(len(iteration_options)):
    #    iteration_options[i] = 2 * iteration_options[i]

    sigma = [1]
    rho   = [0.0]
    alpha = 0.0001
    tau   = 0.5*0.125

    modes = [1]
    kernel_types = [0,1,2,3]

    anisotropicDiffusionImageFilterTest(
        filename,
        modes,
        kernel_types,
        iteration_options,
        sigma,
        rho,
        alpha,
        tau,
        zero_div,
        color_diffusion = True)
    return

# ==============================================================================
#
# Tests not used
#
# ==============================================================================

def vangoghTest1():
    filename = 'van-gogh-1.jpg'
    filepath = os.path.join('.', 'data', filename)
    image = skimage.io.imread(filepath)
    #image = skimage.color.rgb2gray(image)
    image = rgb2gray(image)

    image = image.astype(float)

    zero_div = 0
    iteration_options = [10,50,100,200,250,300,350,400]

    result_directory = FileSystem.mkdir(['.', 'results-anisotropic-diffusion', Path(filename).stem])

    sigma = 0.0
    rho   = 0.0
    alpha = 0.01
    tau   = 0.9*0.125

    modes = [0,1,2]
    modes = [2]
    kernel_types = [3]

    #curses.initscr()
    #curses_win = curses.newwin(3, 79, 0, 0)
    #curses_win.addstr(0, 0, filename)
    #curses_win.refresh()
    anisotropicDiffusionImageFilterTest(
        filename,
        modes,
        kernel_types,
        iteration_options,
        sigma,
        rho,
        alpha,
        tau,
        zero_div)
    return

def vangoghTest2():
    filename = 'van-gogh-2.jpg'
    filepath = os.path.join('.', 'data', filename)
    image = skimage.io.imread(filepath)
    #image = skimage.color.rgb2gray(image)
    image = rgb2gray(image)

    image = image.astype(float)

    zero_div = 1
    iteration_options = [10,50,100,200]

    result_directory = FileSystem.mkdir(['.', 'results-anisotropic-diffusion', Path(filename).stem])

    sigma = 0.0
    rho   = 0.0
    alpha = 0.01
    tau   = 0.9*0.125

    modes = [0,1,2]
    modes = [2]
    kernel_types = [3]

    for mode in modes:
        for kernel_type in kernel_types:
            iterations = iteration_options[-1]
            results_seq = []
            flt0 = anisotropicDiffusionFilter(
                image,
                iterations = iterations,
                sigma = sigma,
                rho   = rho,
                alpha = alpha,
                mode  = mode,
                tau   = tau,
                image_seq = results_seq,
                kernel_type = kernel_type,
                zero_div = zero_div,
            )

            for iterations in iteration_options:
                saveResult(
                    results_seq[iterations],
                    original_filename = filename,
                    result_directory = result_directory,
                    iterations = iterations,
                    sigma = sigma,
                    rho   = rho,
                    alpha = alpha,
                    mode  = mode,
                    tau   = tau,
                    kernel_type = kernel_type,
                    zero_div = zero_div,
                )
            del results_seq

def vangoghTest3() -> None:
    filename = 'van-gogh-road-with-cypress-and-star-tiny.jpg'
    filepath = os.path.join('.', 'data', filename)
    image = skimage.io.imread(filepath)
    #image = rgb2gray(image)

    image = image.astype(float)

    zero_div = 0
    iteration_options = [8, 24, 32, 48, 64]

    result_directory = FileSystem.mkdir(['.', 'results-anisotropic-diffusion', Path(filename).stem])

    sigma = 0.5
    rho   = 0.6
    alpha = 0.01
    C     = 3.315
    tau   = 0.125

    #modes = [DiffusionMode.COHERENCE, DiffusionMode.ALONG]
    #kernel_types = [KernelType.KROON]

    modes = [2]
    kernel_types = [3]

    for mode in modes:
        for kernel_type in kernel_types:
            iterations = iteration_options[-1]
            results_seq = []
            flt0 = anisotropicDiffusionFilter(
                image,
                iterations = iterations,
                sigma = sigma,
                rho   = rho,
                alpha = alpha,
                mode  = mode,
                tau   = tau,
                image_seq = results_seq,
                kernel_type = kernel_type,
                zero_div = zero_div,
            )

            for iterations in iteration_options:
                saveResult(
                    results_seq[iterations],
                    original_filename = filename,
                    result_directory = result_directory,
                    iterations = iterations,
                    sigma = sigma,
                    rho   = rho,
                    alpha = alpha,
                    mode  = mode,
                    tau   = tau,
                    kernel_type = kernel_type,
                    zero_div = zero_div,
                )
            del results_seq
    return

def spiderTest():
    filename = 'spider-cropped-2.jpg'
    filepath = os.path.join('.', 'data', filename)
    image = skimage.io.imread(filepath)
    #image = skimage.color.rgb2gray(image)
    image = rgb2gray(image)

    image = image.astype(float)

    zero_div = 0
    iteration_options = [10,50,100,200]

    result_directory = FileSystem.mkdir(['.', 'results-anisotropic-diffusion', Path(filename).stem])

    sigma = 2.0
    rho   = 1.0
    alpha = 0.01
    tau   = 0.125

    modes = [0,1,2]
    modes = [2]

    for mode in modes:
        for kernel_type in range(1,4):
            #for iterations in iteration_options:
            iterations = iteration_options[-1]
            results_seq = []
            flt0 = anisotropicDiffusionFilter(
                image,
                iterations = iterations,
                sigma = sigma,
                rho   = rho,
                alpha = alpha,
                mode  = mode,
                tau   = tau,
                image_seq = results_seq,
                kernel_type = kernel_type,
                zero_div = zero_div,
            )

            for iterations in iteration_options:
                saveResult(
                    results_seq[iterations],
                    original_filename = filename,
                    result_directory = result_directory,
                    iterations = iterations,
                    sigma = sigma,
                    rho   = rho,
                    alpha = alpha,
                    mode  = mode,
                    tau   = tau,
                    kernel_type = kernel_type,
                    zero_div = zero_div,
                )
            del results_seq

# ==============================================================================
#
# Main
#
# ==============================================================================
if __name__=='__main__':

    print("Make anisotropic filtered images")

    #vangoghColorTest1()
    #vangoghColorTest2()

    #objectsTest()
    #fiberTest()
    fingerprintTest()
    fingerprintArtifactsTest()
    
    #vangoghTest1()
    #vangoghTest2()
    #spiderTest()
    #makeNoisyImagesExamplesGauss()
    #makeNoisyImagesExamplesSpeckle()

    #copyResultsToDirectory()
