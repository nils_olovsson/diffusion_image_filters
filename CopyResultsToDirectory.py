#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

import os

import shutil

from pathlib import Path

import FileSystem

def copyResultsToDirectory():
    resdir = os.path.join('.', 'results-nonlinear-diffusion')
    target = os.path.join(os.sep, 'mnt', 'storage', 'documents', 'www',
                          'AllSystemsPhenomenal', 'articles',
                          'image_smoothing_by_nonlinear_diffusion')

    print(target)
    if os.path.isdir(target):
        print("The target directory exists :)")
    else:
        print("The target directory does not exist :(")

    print(f"Copy reult files to:\n{target}")
    ans = input('Y/N:')
    if ans != 'Y' and ans != 'y':
        return

    target_files   = FileSystem.mkdir([target, 'files'])
    target_video   = FileSystem.mkdir([target, 'files', 'video'])
    target_figures = FileSystem.mkdir([target, 'files', 'figures'])

    filepath_video_0 = os.path.join(resdir, 'diffusion1D', 'video', 'diffusion-1d.mp4')
    filepath_video_1 = os.path.join(resdir, 'diffusion1D', 'video', 'nonlin-diffusion-1d.mp4')
    filepath_video_2 = os.path.join(resdir, 'ct-example', 'video', 'ct-diffusion.mp4')
    shutil.copy(filepath_video_0, target_video)
    shutil.copy(filepath_video_1, target_video)
    shutil.copy(filepath_video_2, target_video)

    filepath_fig_0 = os.path.join(resdir, 'ct-example', 'ct-example.png')
    filepath_fig_1 = FileSystem.getFilesInDirectory(os.path.join(resdir, 'ct-example', 'nonlinear'))
    filepath_fig_1.sort()
    filepath_fig_1 = filepath_fig_1[-1]
    filepath_fig_2 = os.path.join(resdir, 'ct-example', 'ct-gradient-magnitude-diffusivity.png')
    shutil.copy(filepath_fig_0, target_figures)
    shutil.copy(filepath_fig_1, target_figures)
    shutil.copy(filepath_fig_2, target_figures)

    print('Files copied.')
    return

def copyAnisotropicIllustrationsToDirectory():
    resdir = os.path.join('.', 'results-anisotropic-diffusion', 'kernel-illustrations')
    viddir = os.path.join('.', 'results-anisotropic-diffusion', 'video')
    target_root = os.path.join(os.sep, 'mnt', 'storage', 'documents', 'www',
                          'AllSystemsPhenomenal', 'articles',
                          'image_smearing_by_anisotropic_diffusion')

    print(target_root)
    if os.path.isdir(target_root):
        print("The target directory exists :)")
    else:
        print("The target directory does not exist :(")

    print(f"Copy reult files to:\n{target_root}")
    ans = input('Y/N:')
    if ans != 'Y' and ans != 'y':
        return

    target_files   = FileSystem.mkdir([target_root, 'files'])
    #target_video   = FileSystem.mkdir([target, 'files', 'video'])
    #target_figures = FileSystem.mkdir([target, 'files', 'figures'])
    target_kernel = FileSystem.mkdir([target_root, 'files', 'figures', 'kernel-illustrations'])

    files = FileSystem.getFilesInDirectory(resdir)
    for file in files:
        name = Path(file).name
        print(name)
        target_file = os.path.join(target_kernel, name)
        if os.path.isfile(target_file):
            os.remove(target_file)
        shutil.copy(file, target_file)

    target_video = FileSystem.mkdir([target_root, 'files', 'video'])
    files = FileSystem.getFilesInDirectory(viddir)
    for file in files:
        name = Path(file).name
        print(name)
        target_file = os.path.join(target_video, name)
        if os.path.isfile(target_file):
            os.remove(target_file)
        shutil.copy(file, target_file)

    ## Ploughing colorman
    vidimagedir = os.path.join('.', 'results-anisotropic-diffusion', 'van-gogh-field-with-ploughman-tiny-color', '{kroon}-{wckcoherence}-s{01.00}-r{00.00}-a{1:00E-03}-zdiv-{false}')
    target_video = FileSystem.mkdir([target_root, 'files', 'figures','results'])
    files = FileSystem.getFilesInDirectory(vidimagedir)
    files.sort()
    files = [files[0], files[-1]]
    for file in files:
        name = Path(file).name
        print(name)
        target_file = os.path.join(target_video, name)
        if os.path.isfile(target_file):
            os.remove(target_file)
        shutil.copy(file, target_file)

    ## Fingerprint filtering in introduction
    fingerprintdir = os.path.join('.', 'results-anisotropic-diffusion', 'weickert-fingerprint-2', '{kroon}-{wckcoherence}-s{00.50}-r{00.50}-a{1:00E-04}-zdiv-{false}')
    target_video = FileSystem.mkdir([target_root, 'files', 'figures','results'])
    files = FileSystem.getFilesInDirectory(fingerprintdir)
    files.sort()
    files = [files[0], files[-1]]
    for file in files:
        name = Path(file).name
        print(name)
        target_file = os.path.join(target_video, name)
        if os.path.isfile(target_file):
            os.remove(target_file)
        shutil.copy(file, target_file)

    ## Fingerprint with artifacts
    fingerprintdir = os.path.join('.', 'results-anisotropic-diffusion', 'weickert-fingerprint-2', '{scharr}-{wckcoherence}-s{00.50}-r{00.50}-a{1:00E-04}-zdiv-{false}')
    target_video = FileSystem.mkdir([target_root, 'files', 'figures','results'])
    files = FileSystem.getFilesInDirectory(fingerprintdir)
    files.sort()
    files = [files[0], files[-1]]
    for file in files:
        name = Path(file).name
        print(name)
        target_file = os.path.join(target_video, name)
        if os.path.isfile(target_file):
            os.remove(target_file)
        shutil.copy(file, target_file)

    ## Cypress and stars in color for comparing the different kernels
    image_name = 'van-gogh-road-with-cypress-and-star-tiny-color'
    rootdir = os.path.join('.', 'results-anisotropic-diffusion', image_name)
    iterations = [0,40,100,100,500]
    mode_strings = ['fd', 'fd','sobel','scharr','kroon']
    for i, mstr in enumerate(mode_strings):
        mode_strings[i] = f'{{{mstr}}}' + '-{wckcoherence}-s{01.00}-r{00.00}-a{1:00E-04}-zdiv-{false}'
    
    targetdir = FileSystem.mkdir([target_root, 'files', 'figures','results','kernel-compare'])
    for i in range(len(mode_strings)):
        sourcedir = os.path.join(rootdir, mode_strings[i])
        filename = image_name + '-' + mode_strings[i] + f'-it-{iterations[i]:04d}.png'
        filepath = os.path.join(sourcedir, filename)
        if not os.path.isfile(filepath):
            print(f"error: file not found.    \n{filepath}")
        else:
            target_filepath = os.path.join(targetdir, filename)
            if os.path.isfile(target_filepath):
                os.remove(target_filepath)
            shutil.copy(filepath, target_filepath)
            print(Path(filepath).name)

    """
    filepath_video_0 = os.path.join(resdir, 'diffusion1D', 'video', 'diffusion-1d.mp4')
    filepath_video_1 = os.path.join(resdir, 'diffusion1D', 'video', 'nonlin-diffusion-1d.mp4')
    filepath_video_2 = os.path.join(resdir, 'ct-example', 'video', 'ct-diffusion.mp4')
    shutil.copy(filepath_video_0, target_video)
    shutil.copy(filepath_video_1, target_video)
    shutil.copy(filepath_video_2, target_video)
    filepath_fig_0 = os.path.join(resdir, 'ct-example', 'ct-example.png')
    filepath_fig_1 = FileSystem.getFilesInDirectory(os.path.join(resdir, 'ct-example', 'nonlinear'))
    filepath_fig_1.sort()
    filepath_fig_1 = filepath_fig_1[-1]
    filepath_fig_2 = os.path.join(resdir, 'ct-example', 'ct-gradient-magnitude-diffusivity.png')
    shutil.copy(filepath_fig_0, target_figures)
    shutil.copy(filepath_fig_1, target_figures)
    shutil.copy(filepath_fig_2, target_figures)
    """

    print('Files copied.')
    return

# ==============================================================================
#
# Main
#
# ==============================================================================
if __name__=='__main__':
    #copyResultsToDirectory()
    copyAnisotropicIllustrationsToDirectory()
