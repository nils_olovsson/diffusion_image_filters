#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

import os

from typing import Tuple, List

import math

import numpy as np
import scipy.signal

import skimage.color

from DiffusionImageFilter import DiffusionImageFilterUpdate

def computeGradMagnAndDiffusivity(u: np.ndarray, lamb: float):
    """
        For examples.
        Compute the nonlinear, gradient derived, diffusivity.
        Returns both the gradient magnitude and diffusivity
        as two 2D images.
    """
    shape = u.shape
    if len(shape) > 2 and shape[2] > 1:
        print("RGB to gray")
        u = skimage.color.rgb2gray(u)
    gradkernelx = 0.5 * np.array([[ 0.0, 0.0, 0.0],
                                  [-1.0, 0.0, 1.0],
                                  [ 0.0, 0.0, 0.0]])
    gradkernely = 0.5 * np.array([[ 0.0,-1.0, 0.0],
                                  [ 0.0, 0.0, 0.0],
                                  [ 0.0, 1.0, 0.0]])
    gradx  = scipy.signal.convolve2d(u, gradkernelx, boundary='symm')
    grady  = scipy.signal.convolve2d(u, gradkernely, boundary='symm')
    gradm2 = np.power(gradx, 2) + np.power(grady, 2)
    g = 1.0 / np.sqrt(1.0 + gradm2 / (lamb*lamb))

    gradm2 = gradm2[1:-1, 1:-1]
    g = g[1:-1, 1:-1]

    print(u.shape)
    print(gradm2.shape)
    print(g.shape)

    return gradm2, g

def linearDiffusionFilter(image: np.ndarray,
                          iterations = 10,
                          g = 1.0,
                          tau = 0.125,
                          image_seq = None):
    """
        Execute linear, isotropic, smoothing filter on image.
        This smoothing method uses diffusion that blurs edges.
    """

    def computeUpdate(u: np.ndarray, g: float):
        """
            Comput the update for the next iteration using the spatial
            derivatives.
        """
        update = np.zeros(u.shape, dtype=float)
        u = np.pad(u, pad_width=1, mode='constant')

        return DiffusionImageFilterUpdate.computeUpdateLinear1Ch(u, update, g)
        """
        for i in range(1, u.shape[1]-1):
            for j in range(1, u.shape[0]-1):

                g_pj = g
                g_nj = g
                g_ip = g
                g_in = g

                if i==u.shape[1]-2:
                    g_pj = 0
                if i==1:
                    g_nj = 0
                if j==u.shape[0]-2:
                    g_ip = 0
                if j==1:
                    g_in = 0

                ux0 =   g_pj * (u[j, i+1] - u[j, i])
                ux1 = - g_nj * (u[j, i]   - u[j, i-1])
                uy0 =   g_ip * (u[j+1, i] - u[j, i])
                uy1 = - g_in * (u[j, i]   - u[j-1, i])

                update[j-1,i-1] = ux0 + ux1 + uy0 + uy1
        return update
        """

    u = np.copy(image)
    if len(u.shape) > 2 and u.shape[2] == 1:
        u = np.reshape(u, (u.shape[0], u.shape[1]))
    if image_seq != None:
        image_seq.append(np.copy(u))

    for i in range(iterations):
        print(f"Iteration: {i+1}/{iterations}")
        update = computeUpdate(u, g)
        u = u + tau * update
        if image_seq != None:
            image_seq.append(np.copy(u))
    return u


def nonlinearDiffusionFilter(image: np.ndarray,
                             iterations = 10,
                             lamb = 1.0,
                             tau = 0.125,
                             image_seq = None):
    """
        Execute nonlinear, isotropic, smoothing filter on image.
        The method is described in the 1990 paper by Perona and Malik.
        This smoothing method uses diffusion that preserves edges.
    """

    def computeUpdate(u: np.ndarray, g: np.ndarray):
        """
            Comput the update for the next iteration using the spatial
            derivatives.
        """
        update = np.zeros(u.shape, dtype=float)
        u = np.pad(u, pad_width=1, mode='constant')
        g = np.pad(g, pad_width=1, mode='constant')
        return DiffusionImageFilterUpdate.computeUpdateNonLinear1Ch(u, update, g)

        """
        for i in range(1, u.shape[1]-1):
            for j in range(1, u.shape[0]-1):

                g_pj = math.sqrt(g[j, i+1] * g[j, i])
                g_nj = math.sqrt(g[j, i-1] * g[j, i])
                g_ip = math.sqrt(g[j+1, i] * g[j, i])
                g_in = math.sqrt(g[j-1, 1] * g[j, i])

                if i==u.shape[1]-2:
                    g_pj = 0
                if i==1:
                    g_nj = 0
                if j==u.shape[0]-2:
                    g_ip = 0
                if j==1:
                    g_in = 0

                ux0 =   g_pj * (u[j, i+1] - u[j, i])
                ux1 = - g_nj * (u[j, i]   - u[j, i-1])
                uy0 =   g_ip * (u[j+1, i] - u[j, i])
                uy1 = - g_in * (u[j, i]   - u[j-1, i])

                update[j-1,i-1] = ux0 + ux1 + uy0 + uy1
        return update
        """

    def computeDiffusivity(u: np.ndarray, lamb: float):
        """
            Compute the nonlinear, gradient derived, diffusivity.
        """
        shape = u.shape
        if len(shape) > 2 and shape[2] > 1:
            print("RGB to gray")
            u = skimage.color.rgb2gray(u)
        gradkernelx = 0.5 * np.array([[ 0.0, 0.0, 0.0],
                                      [-1.0, 0.0, 1.0],
                                      [ 0.0, 0.0, 0.0]])
        gradkernely = 0.5 * np.array([[ 0.0,-1.0, 0.0],
                                      [ 0.0, 0.0, 0.0],
                                      [ 0.0, 1.0, 0.0]])
        gradx  = scipy.signal.convolve2d(u, gradkernelx, boundary='symm')
        grady  = scipy.signal.convolve2d(u, gradkernely, boundary='symm')
        gradm2 = np.power(gradx, 2) + np.power(grady, 2)
        g = 1.0 / np.sqrt(1.0 + gradm2 / (lamb*lamb))
        return g

    u = np.copy(image)
    if len(u.shape) > 2 and u.shape[2] == 1:
        u = np.reshape(u, (u.shape[0], u.shape[1]))
    if image_seq != None:
        image_seq.append(np.copy(u))

    for i in range(iterations):
        print(f"Iteration: {i+1}/{iterations}")
        g = computeDiffusivity(u, lamb)
        update = computeUpdate(u, g)
        u = u + tau * update
        if image_seq != None:
            image_seq.append(np.copy(u))
    return u

