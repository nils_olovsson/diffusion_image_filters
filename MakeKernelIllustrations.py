#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: https://all-systems-phenomenal.com
date:    2022
license: MIT

"""

import os
import sys

import shutil
from pathlib import Path

from typing import Tuple, List

import numpy as np
import scipy.signal

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
#import matplotlib.ticker as ticker
#import matplotlib.gridspec as gridspec
#import matplotlib.patheffects as patheffects

import FileSystem
import LatexPreamble

class ArrowPatch:
    def __init__(self, p0, p1, ax0, ax1=None, arrowstyle=None, color='k', zorder=10):
        self.p0 = p0
        self.p1 = p1

        self.ax0 = ax0
        self.ax1 = ax1

        self.color = color
        self.zorder = zorder

        if not arrowstyle:
            self.arrowstyle = "Simple, tail_width=2.0, head_width=12, head_length=12"
        else:
           self.arrowstyle = arrowstyle 
        self.connectionstyle = "arc3, rad=0.0"


    def setConnectionStyle(self, style):
        self.connectionstyle = style

    def setArrowStyle(self, style):
        self.arrowstyle = style

    def isConnectionPatch(self):
        return self.ax1 != None

    def plot(self, fig):
        kw = dict(arrowstyle=self.arrowstyle, color=self.color, zorder=self.zorder)

        if self.isConnectionPatch():
            print("Hello")
            a = mpatches.ConnectionPatch(xyA = self.p0,
                                         xyB = self.p1,
                                         coordsA = self.ax0.transData,
                                         coordsB = self.ax1.transData,
                                         connectionstyle = self.connectionstyle, **kw)
            fig.add_artist(a)
        else:
            style = self.arrowstyle
            kw = dict(arrowstyle=style, color=self.color, zorder=self.zorder, capstyle='projecting', joinstyle='round')
            a = mpatches.FancyArrowPatch(self.p0, self.p1,
                                         connectionstyle = self.connectionstyle, **kw)
            self.ax0.add_patch(a)

def kernelIllustration(
    kernel: np.ndarray,
    scale: int,
    text: str,
    directory: str,
    filename: str,
):
    """
    """

    #print(kernel)

    LatexPreamble.latexMatplotlibPreample()

    figsize = kernel.shape[0] + 0.2
    fig= plt.figure(figsize=(figsize, figsize), dpi=300)
    ax = fig.gca()

    print(kernel.shape[0])

    for r in range(kernel.shape[0]+1):
        for c in range(kernel.shape[0]+1):
            
            x0 = 0
            x1 = kernel.shape[0]
            y0 = 0
            y1 = kernel.shape[0]

            lines = []
            l0 = mlines.Line2D([x0, x1], [r, r])
            l1 = mlines.Line2D([c, c], [y0, y1])
            lines.append(l0)
            lines.append(l1)
            #lines[1] = mlines.Line2D([p2[0], p3[0]], [p2[1], p3[1]])
            #lines[2] = mlines.Line2D([p1[0], p3[0]], [p1[1], p3[1]])
            #lines[3] = mlines.Line2D([p4[0], p5[0]], [p4[1], p5[1]])
            for l in lines:
                l.set_color('k')
                l.zorder = 2
                l.set_solid_capstyle('round')
                l.set_solid_joinstyle('round')
                ax.add_line(l)

    """
    lines = []
    l0 = mlines.Line2D([-0.5, 0.5], [kernel.shape[0]+0.5, kernel.shape[0]+0.5])
    l1 = mlines.Line2D([-0.5,-0.5], [kernel.shape[0]+0.5, kernel.shape[0]-0.5])
    #l1 = mlines.Line2D([, ], [, ])
    lines.append(l0)
    lines.append(l1)
    for l in lines:
        l.set_color('k')
        l.zorder = 2
        l.set_solid_capstyle('round')
        l.set_solid_joinstyle('round')
        ax.add_line(l)
    """

    w = 0.06
    p0 = np.array([0-0.5-w, kernel.shape[0]+0.5])
    p1 = p0 + np.array([1.0,  0.0])
    p2 = np.array([0-0.5, kernel.shape[0]+0.5+w])
    p3 = p2 + np.array([0.0, -1.0])
    a0 = ArrowPatch(p0, p1, ax)
    a1 = ArrowPatch(p2, p3, ax)

    a0.plot(fig)
    a1.plot(fig)

    fontsize = 12
    ax.text( 0.7, p0[1]-0.15, r'\textbf{x}', va="center", ha="right", fontsize=2*fontsize)
    ax.text(-0.2, p3[1]-0.1, r'\textbf{y}', va="center", ha="right", fontsize=2*fontsize)

    #ax.arrow(0-0.5, kernel.shape[0]+0.5, 0.5, 0.0)
    #ax.annotate("", xy=(0-0.5, kernel.shape[0]+0.5), xytext=(0.5, 0.0), arrowprops=dict(arrowstyle="->"))

    for r in range(kernel.shape[0]):
        for c in range(kernel.shape[0]):

            #ax.text(c+0.5, r+0.5, f'({r}, {c})', va="center", ha="center", fontsize=18)
            v = kernel[r,c]
            s = ''
            fontsize = 16
            if kernel.dtype == int:
                s = r'\textbf{'+ f'{v:d}' + '}'
            else:
                s = f'{v:.1e}'
                fontsize = 12
            ax.text(c+0.5, r+0.5, s, va="center", ha="center", fontsize=fontsize)

    s = text
    s += r' = \mathbf{\frac{1}{' + f'{scale}' + r'}}\times$'
    #s = r'$k_{xy}^{Scharr} = \mathbf{\frac{1}{' + f'{scale}' + r'}}\times$'
    #print(s)

    fontsize = 16
    ax.text(0, kernel.shape[0]//2 + 0.5, s, va="center", ha="right", fontsize=2*fontsize)

    ax.set_axis_off()

    #ax.scatter(1,1)
    ax.plot([0,0],[0,0])

    fig.canvas.flush_events()
    fig.canvas.draw()

    filepath = os.path.join(directory, filename)
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0)
    #plt.savefig(filepath, pad_inches=0)

    return

def kernelIllustrationStr(
    kernel: List,
    scale: int,
    text: str,
    directory: str,
    filename: str,
):
    """
    """

    #print(kernel)

    LatexPreamble.latexMatplotlibPreample()

    n = len(kernel)

    figsize = n + 0.2
    fig= plt.figure(figsize=(figsize, figsize), dpi=300)
    ax: plt.Axes = fig.gca()

    for r in range(n+1):
        for c in range(n+1):
            
            x0 = 0
            x1 = n
            y0 = 0
            y1 = n

            lines = []
            l0 = mlines.Line2D([x0, x1], [r, r])
            l1 = mlines.Line2D([c, c], [y0, y1])
            lines.append(l0)
            lines.append(l1)
            #lines[1] = mlines.Line2D([p2[0], p3[0]], [p2[1], p3[1]])
            #lines[2] = mlines.Line2D([p1[0], p3[0]], [p1[1], p3[1]])
            #lines[3] = mlines.Line2D([p4[0], p5[0]], [p4[1], p5[1]])
            for l in lines:
                l.set_color('k')
                l.zorder = 2
                l.set_solid_capstyle('round')
                l.set_solid_joinstyle('round')
                ax.add_line(l)

    lines = []
    #l0 = mlines.Line2D([-0.5, 0.5], [n+0.5, n+0.5])
    #l1 = mlines.Line2D([-0.5,-0.5], [n+0.5, n-0.5])
    #l1 = mlines.Line2D([, ], [, ])
    #lines.append(l0)
    #lines.append(l1)
    """
    for l in lines:
        l.set_color('k')
        l.zorder = 2
        l.set_solid_capstyle('round')
        l.set_solid_joinstyle('round')
        ax.add_line(l)
    """
    #ax.arrow(-0.5, 0.5, 0.5, 0.0)
    #ax.annotate("", xy=(0.5, 0.5), xytext=(0, 0), arrowprops=dict(arrowstyle="->"))

    n = len(kernel)

    w = 0.06
    p0 = np.array([0-0.5-w, n+0.5])
    p1 = p0 + np.array([1.0,  0.0])
    p2 = np.array([0-0.5, n+0.5+w])
    p3 = p2 + np.array([0.0, -1.0])
    a0 = ArrowPatch(p0, p1, ax)
    a1 = ArrowPatch(p2, p3, ax)

    a0.plot(fig)
    a1.plot(fig)

    fontsize = 12
    ax.text( 0.7, p0[1]-0.15, r'\textbf{x}', va="center", ha="right", fontsize=2*fontsize)
    ax.text(-0.2, p3[1]-0.1, r'\textbf{y}', va="center", ha="right", fontsize=2*fontsize)

    for r in range(n):
        for c in range(n):

            #ax.text(c+0.5, r+0.5, f'({r}, {c})', va="center", ha="center", fontsize=18)
            #v = kernel[r,c]
            s = kernel[r][c]
            fontsize = 16
            #if kernel.dtype == int:
            #    s = r'\textbf{'+ f'{v:d}' + '}'
            #else:
            #    s = f'{v:.1E}'
            #    fontsize = 12
            ax.text(c+0.5, r+0.5, s, va="center", ha="center", fontsize=fontsize)

    s = text
    s += r' = \mathbf{\frac{1}{' + f'{scale}' + r'}}\times$'
    #s = r'$k_{xy}^{Scharr} = \mathbf{\frac{1}{' + f'{scale}' + r'}}\times$'
    print(s)

    fontsize = 16
    ax.text(0, n//2 + 0.5, s, va="center", ha="right", fontsize=2*fontsize)

    ax.set_axis_off()

    #ax.scatter(1,1)
    ax.plot([0,0],[0,0])

    fig.canvas.flush_events()
    fig.canvas.draw()

    filepath = os.path.join(directory, filename)
    plt.savefig(filepath, bbox_inches='tight', pad_inches=0)
    #plt.savefig(filepath, pad_inches=0)

    return

def makeKernelIllustrations():
    """
    """

    directory = FileSystem.mkdir(['.', 'results-anisotropic-diffusion', 'kernel-illustrations'])

    # --------------------------------------------
    # Finite differences filters
    # --------------------------------------------

    fdiff_dx_s  = 2
    fdiff_dxy_s = 4
    fdiff_dxx_s = 4

    fdiff_dx_k = np.array([[ 0, 0, 0],
                           [-1, 0, 1],
                           [ 0, 0, 0]], dtype=int)

    fdiff_dxx_k = np.array([[0, 0, 0, 0, 0],
                            [0, 0, 0, 0, 0],
                            [0, 1,-2, 1, 0],
                            [0, 0, 0, 0, 0],
                            [0, 0, 0, 0, 0]], dtype=int)

    fdiff_dxy_k = np.array([[0, 0, 0, 0, 0],
                            [0, 1, 0,-1, 0],
                            [0, 0, 0, 0, 0],
                            [0,-1, 0, 1, 0],
                            [0, 0, 0, 0, 0]], dtype=int)

    text0 = r'$k_{x}^{FD}'
    text1 = r'$k_{xx}^{FD}'
    text2 = r'$k_{xy}^{FD}'

    kernelIllustration(fdiff_dx_k,  fdiff_dx_s,  text0, directory, 'fdiff-dx.svg')
    kernelIllustration(fdiff_dxx_k, fdiff_dxx_s, text1, directory, 'fdiff-dxx.svg')
    kernelIllustration(fdiff_dxy_k, fdiff_dxy_s, text2, directory, 'fdiff-dxy.svg')

    # --------------------------------------------
    # Sobel filters
    # --------------------------------------------

    sobel_dx_s  = 8
    sobel_dxy_s = 64
    sobel_dxx_s = 64

    sobel_dx_k = np.array([ [-1, 0, 1],
                            [-2, 0, 2],
                            [-1, 0, 1]], dtype=int)

    sobel_dxx_k = np.array([[1, 0,  -2, 0, 1],
                            [4, 0,  -8, 0, 4],
                            [6, 0, -12, 0, 6],
                            [4, 0,  -8, 0, 4],
                            [1, 0,  -2, 0, 1]], dtype=int)

    sobel_dxy_k = np.array([[ 1,  2, 0, -2, -1],
                            [ 2,  4, 0, -4, -2],
                            [ 0,  0, 0,  0,  0],
                            [-2, -4, 0,  4,  2],
                            [-1, -2, 0,  2,  1]], dtype=int)

    text0 = r'$k_{x}^{Sobel}'
    text1 = r'$k_{xx}^{Sobel}'
    text2 = r'$k_{xy}^{Sobel}'

    kernelIllustration(sobel_dx_k,  sobel_dx_s,  text0, directory, 'sobel-dx.svg')
    kernelIllustration(sobel_dxx_k, sobel_dxx_s, text1, directory, 'sobel-dxx.svg')
    kernelIllustration(sobel_dxy_k, sobel_dxy_s, text2, directory, 'sobel-dxy.svg')

    # --------------------------------------------
    # Scharr filters
    # --------------------------------------------

    """
    # This version of the Scharr filter is mentioned on the Wikipedia page
    scharr_dx_s  = 512
    scharr_dxy_s = 262144
    scharr_dxx_s = 262144

    u0 = 47
    u1 = 162

    v0 = 2209
    v1 = 4418
    v2 = 15228
    v3 = 30456
    v4 = 30662
    v5 = 61324

    w0 = 2209
    w1 = 7614
    w2 = 26244
    """

    scharr_dx_s  = 32
    scharr_dxy_s = 32*32
    scharr_dxx_s = 32*32

    u0 = 3
    u1 = 10

    v0 = 9
    v1 = 18
    v2 = 60
    v3 = 120
    v4 = 118
    v5 = 236

    w0 = 9
    w1 = 30
    w2 = 100

    scharr_dx_k = np.array([[-u0, 0, u0],
                            [-u1, 0, u1],
                            [-u0, 0, u0]], dtype=int)

    scharr_dxx_k = np.array([[v0, 0, -v1, 0, v0],
                             [v2, 0, -v3, 0, v2],
                             [v4, 0, -v5, 0, v4],
                             [v2, 0, -v3, 0, v2],
                             [v0, 0, -v1, 0, v0]], dtype=int)

    scharr_dxy_k = np.array([[ w0,  w1, 0, -w1, -w0],
                             [ w1,  w2, 0, -w2, -w1],
                             [  0,   0, 0,   0,   0],
                             [-w1, -w2, 0,  w2,  w1],
                             [-w0, -w1, 0,  w1,  w0]], dtype=int)

    text0 = r'$k_{x}^{Scharr}'
    text1 = r'$k_{xx}^{Scharr}'
    text2 = r'$k_{xy}^{Scharr}'

    kernelIllustration(scharr_dx_k,  scharr_dx_s,  text0, directory, 'scharr-dx.svg')
    kernelIllustration(scharr_dxx_k, scharr_dxx_s, text1, directory, 'scharr-dxx.svg')
    kernelIllustration(scharr_dxy_k, scharr_dxy_s, text2, directory, 'scharr-dxy.svg')

    # --------------------------------------------
    # Kroon filters
    # --------------------------------------------

    kroon_dx_s  = 1
    kroon_dxy_s = 1
    kroon_dxx_s = 1

    p = np.array([0.00549691757211334,
                  4.75686155670860e-10,
                  3.15405721902292e-11,
                  0.00731109320628158,
                  1.40937549145842e-10,
                  0.0876322157772825,
                  0.0256808495553998,
                  5.87110587298283e-11,
                  0.171008417902939,
                  3.80805359553021e-12,
                  9.86953381462523e-12,
                  0.0231020787600445,
                  0.00638922328831119,
                  0.0350184289706385], dtype=np.double)

    # The papers by Kroon as well as the matlab code starts indexing from 1 so
    # do expand array into variables for easier readability
    p01 = p[0]
    p02 = p[1]
    p03 = p[2]
    p04 = p[3]
    p05 = p[4]
    p06 = p[5]
    p07 = p[6]
    p08 = p[7]
    p09 = p[8]
    p10 = p[9]
    p11 = p[10]
    p12 = p[11]
    p13 = p[12]
    p14 = p[13]

    kroon_dx_k = np.array([[-p13, 0, p13],
                           [-p14, 0, p14],
                           [-p13, 0, p13]], dtype=float)

    kroon_dxx_k = np.array([[p01, p04, -p07, p04, p01],
                            [p02, p05, -p08, p05, p02],
                            [p03, p06, -p09, p06, p03],
                            [p02, p05, -p08, p05, p02],
                            [p01, p04, -p07, p04, p01]], dtype=float)

    kroon_dxy_k = np.array([[ p10,  p11, 0, -p11, -p10],
                            [ p11,  p12, 0, -p12, -p11],
                            [   0,    0, 0,    0,    0],
                            [-p11, -p12, 0,  p12,  p11],
                            [-p10, -p11, 0,  p11,  p10]], dtype=float)

    # String version of Kroon kernels
    kroon_dx_str = [['$-p_{13}$', '0', '$p_{13}$'],
                    ['$-p_{14}$', '0', '$p_{14}$'],
                    ['$-p_{13}$', '0', '$p_{13}$']]

    kroon_dxx_str = [['$p_{1}$', '$p_{4}$', '$-p_{7}$', '$p_{4}$', '$p_{1}$'],
                     ['$p_{2}$', '$p_{5}$', '$-p_{8}$', '$p_{5}$', '$p_{2}$'],
                     ['$p_{3}$', '$p_{6}$', '$-p_{9}$', '$p_{6}$', '$p_{3}$'],
                     ['$p_{2}$', '$p_{5}$', '$-p_{8}$', '$p_{5}$', '$p_{2}$'],
                     ['$p_{1}$', '$p_{4}$', '$-p_{7}$', '$p_{4}$', '$p_{1}$']]

    kroon_dxy_str = [[ '$p_{10}$',  '$p_{11}$', '0', '$-p_{11}$', '$-p_{10}$'],
                     [ '$p_{11}$',  '$p_{12}$', '0', '$-p_{12}$', '$-p_{11}$'],
                     [   '0',    '0', '0',    '0',    '0'],
                     ['$-p_{11}$', '$-p_{12}$', '0',  '$p_{12}$',  '$p_{11}$'],
                     ['$-p_{10}$', '$-p_{11}$', '0',  '$p_{11}$',  '$p_{10}$']]

    text0 = r'$k_{x}^{Kroon}'
    text1 = r'$k_{xx}^{Kroon}'
    text2 = r'$k_{xy}^{Kroon}'

    kernelIllustration(kroon_dx_k,  kroon_dx_s,  text0, directory, 'kroon-dx.svg')
    kernelIllustration(kroon_dxx_k, kroon_dxx_s, text1, directory, 'kroon-dxx.svg')
    kernelIllustration(kroon_dxy_k, kroon_dxy_s, text2, directory, 'kroon-dxy.svg')

    kernelIllustrationStr(kroon_dx_str,  kroon_dx_s,  text0, directory, 'kroon-str-dx.svg')
    kernelIllustrationStr(kroon_dxx_str, kroon_dxx_s, text1, directory, 'kroon-str-dxx.svg')
    kernelIllustrationStr(kroon_dxy_str, kroon_dxy_s, text2, directory, 'kroon-str-dxy.svg')

    for i, v in enumerate(p):
        print(f"{i+1: 2d}: {v:.2E}")

    return

if __name__ == '__main__':
    makeKernelIllustrations()
